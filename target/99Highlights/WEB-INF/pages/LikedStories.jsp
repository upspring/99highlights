<%@ page import="com.upspring.highlights.domain.Home" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Sathya
  Date: 15/06/20
  Time: 12:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>99 Highlights</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="<%=request.getContextPath()%>/resources/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<%=request.getContextPath()%>/resources/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<%=request.getContextPath()%>/resources/img/favicon-16x16.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style-card.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/content-loader.css">
    <style>
        #tags{
            float:left;
            padding:5px;
            font-family:Arial;
        }
        span{
            cursor:pointer;
            display:block;
            float:left;
            color:#222;
            background-color: #fff;
            border-radius: 5px;
            padding:3px 8px 3px 8px;
            margin:5px;
            font-size: 11px;
            border: 1px solid #222;
            text-transform: uppercase;
            letter-spacing: 2px;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        span:hover{
            background-color: #222;
            color: white;
            border: 1px solid #222;
        }
        .tags-list {
            display: inline-block;
            float: left;
            padding-top: 5px;
            padding-right: 15px;
        }
        /* span:after{
           position:absolute;
           content:"×";
           border-radius: 50%;
           padding:1px 5px;
           margin-left:4px;
           font-size:13px;
           background-color:rgba(0, 0, 0, 0.1);
         }
         */
        input{
            background:#eee;
            border:0;
            margin:4px;
            padding:7px;
            width:auto;
        }
        .hidden {
            display: none;
        }

        .btn-outline-ext {
            border: 1px solid #222;
            color: #222;
            padding: 05px 15px 05px 15px;
            border-radius: 5px;
            cursor: pointer;
        }
        .btn-outline-ext a{
            color: #222;
        }
        .btn-outline-ext a:hover{
            color: #222;
            text-decoration: none;
        }
        .btn-outline-ext:hover {
            border: 1px solid #222;
            color: #222;
            background-color: transparent;
        }
        .btn-outline-ext:focus {
            opacity: 0.7;
            border: 1px solid #222 !important;
            color: #222 !important;
            background-color: white !important;
            outline: 1px solid #222 !important;
            border-radius: 3px;
        }
        .loading-img {
            display: block !important;
            margin: 0 auto !important;

        }
    </style>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159030309-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-159030309-1');
    </script>


</head>




<div class="nav-custom">
    <nav class="navbar navbar-expand-lg navbar-light " style="background-color: white">
        <!--    <a class="navbar-brand" href="/"><img src="<%=request.getContextPath()%>/resources/img/logo-6.png" alt="logo-img" style="width: 150px; height: 45px"></a>-->
        <a class="navbar-brand" href="/"><img src="<%=request.getContextPath()%>/resources/img/logo-new-1.png" alt="logo-img" title="Towards an abridged internet" style="width: 190px; height: 50px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <form class="form-inline my-2 my-lg-0" data-toggle="modal" data-target="#login-modal">
                <div id="myBtn" class="btn btn-outline-login my-2 my-sm-0 mr-3" style="display: none">Login</div>
                <div class="btn-group mr-3" id="login" style="display: none">
                    <div class="btn btn-outline-loginusr dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user pr-2"></i>
                        <%=request.getSession().getAttribute("userName")%>
                    </div>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" onclick=" return logout()">Logout</a>
                        <a class="dropdown-item" href="#" id="about">About</a>
                    </div>
                </div>
                <!--<button class="btn btn-outline-success my-2 my-sm-0 mr-5" type="submit" id="myBtn">Login</button>-->
                <%
                    System.out.println("emil id in jsp"+request.getSession().getAttribute("email").toString().length());
                    if(request.getSession().getAttribute("email").toString().length() <= 0 )
                    {
                %>
                <script>
                    document.getElementById("myBtn").style.display="block";
                </script>
                <%
                    }
                %>


                <%
                    if(request.getSession().getAttribute("email").toString().length() > 0)
                    {
                %>
                <script>
                    document.getElementById("login").style.display="block";
                </script>
                <%
                    }
                %>
                <div class="btn btn-outline-ext my-2 my-sm-0 mr-3" id="chrome-id"><a href="https://chrome.google.com/webstore/detail/light-up-the-web/afabncndnlipbonmgbjjcfddjhpmlgeo?utm_source=gmail" target="_blank"><img src="<%=request.getContextPath()%>/resources/img/chrome-icon.png" alt="chrome-icon" width="30px"; height="20px" class="pr-2">Get Extension</a></div>
                <div id="add-url" class="btn btn-outline-login my-2 my-sm-0 mr-3">Add URL</div>
            </form>



            <div id="modal-about" class="modal-about">
                <!-- Modal content -->
                <div class="modal-content-about">
                    <p class="close" id="close-about">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>About</h4>
                        </div>
                        <div class="modal-content-body">
                            <div class="about-container">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal-login" class="modal-login">
                <!-- Modal content -->
                <div class="modal-content-login">
                    <p class="close" id="close-google">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>Login</h4>
                        </div>
                        <div class="modal-content-body">
                            <img src="<%=request.getContextPath()%>/resources/img/login-modal-blue.png" alt="google-login" style="height: 110px; width: 300px;" onclick="login()">
                        </div>
                    </div>
                </div>

            </div>
            <div id="modal-add-url" class="modal-login">
                <!-- Modal content -->
                <div class="modal-content-login">
                    <p class="close" id="close-url">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>Add URL</h4>
                        </div>
                        <div class="modal-content-body pl-4 pr-4 pt-2 pb-3">
                            <!--<img src="img/login-modal-blue.png" alt="google-login" style="height: 110px; width: 300px;">-->
                            <p class="pl-3">Enter URL:</p>
                            <input class="ml-3 mb-3" type="text" name="URL"  id="URL" placeholder="Enter URL">
                            <p class="pl-3">Enter Your Tags:</p>
                            <input class="ml-3 mb-2" type="text" name="Tags" placeholder="Enter Your Tags" id="tags-input-url">
                            <input type="submit" class="url-submit mt-3" value="Submit" id="submit-url"   />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </nav>
</div>
<nav class="navbar navbar-expand-lg navbar-dark navbar-survival101 bg-lite" style="margin-top: 60px;">
    <div class="container-nav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link " href="<%=request.getContextPath()%>/">Trending</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  " href="<%=request.getContextPath()%>/New">New</a>
                </li>

                <%
                    if(request.getSession().getAttribute("email").toString().length() > 0)
                    {
                %>
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/MyStories">My Stories</a>
                </li>


                <li class="nav-item">
                    <a class="nav-link active" href="<%=request.getContextPath()%>/LikedStories">Liked Stories</a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>

</nav>


<div class="container">
    <div class="article-container ml-4" id="content-loader">
        <%for (int  i = 1; i <= 3; i++){ %>
        <div class="article-box">
            <!-- Card Header-->
            <div class="article-img-cnt"></div>
            <div class="articles-img">
            </div>
            <!-- Card Body -->
            <div class="article-box-content">
                <div class="article-header-cnt"></div>
                <div class="article-header-cnt"></div>
                <div class="article-header-cnt"></div>
                <div class="article-header-cnt"></div>
                <div class="tags-header-cnt"></div>
                <div class="tags-cnt"></div>
                <div class="tags-cnt"></div>
                <div class="tags-cnt"></div>
                <!-- Publisher Info-->
                <div class="posted-by-cnt"></div>
                <!-- Card Footer-->
                <div class="article-shadow"></div>
                <div class="article-action">
                    <div class="article-section-footer">
                        <div class="footer-icon-cnt"></div>
                        <div class="footer-icon-cnt"></div>
                        <div class="footer-icon-cnt"></div>
                        <div class="footer-icon-cnt"></div>
                        <div class="footer-icon-cnt"></div>
                    </div>
                </div>
            </div>
            <div class="box-shadow"></div>
        </div>
        <%}%>
    </div>
    <div class="article-container ml-4" id="main main-content">

        <%
            List<Home> highlightsList = (List<Home>) session.getAttribute("99highlights");
            System.out.println("jsp list------->"+highlightsList.size());

            if(highlightsList.size()>0)
            {

                System.out.println("list of grid is----->success");


                for ( int i =  0; i < highlightsList.size(); i++ )
                {
                    Home highlights =highlightsList.get(i);

        %>

        <div class="article-box main" id=card_<%=highlights.getId()%>>
            <div class="articles-img">
                <img src="<%=highlights.getImage_url()%>"  alt="Image-card" style="height:200px">
            </div>
            <div class="article-box-content">
                <a href="<%=highlights.getUrl()%>" target="_blank"><h5><%=highlights.getTitle()%></h5></a>
                <div class="tag-container" style="display: inline-block;">
                    <div id=tags_<%=highlights.getId()%> class="tags">
                        <p class="tags-list"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                  width="24" height="24"
                                                  viewBox="0 0 172 172"
                                                  style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#222222"><path d="M85.355,6.88c-3.07719,0.13438 -6.00656,0.73906 -8.2775,3.01l-67.51,67.51c-1.70656,1.70656 -2.6875,4.03125 -2.6875,6.45c0,2.41875 0.98094,4.63594 2.6875,6.3425l11.395,11.395c0.57781,-3.17125 1.97531,-6.06031 4.3,-8.385l67.6175,-67.51c4.99875,-5.01219 11.59656,-5.0525 15.5875,-5.0525h36.0125v-2.9025c0,-5.99312 -4.86437,-10.8575 -10.8575,-10.8575h-45.15c-1.03469,0 -2.09625,-0.04031 -3.1175,0zM105.995,27.52c-3.07719,0.13438 -6.00656,0.73906 -8.2775,3.01l-67.51,67.51c-1.70656,1.70656 -2.6875,4.03125 -2.6875,6.45c0,2.41875 0.98094,4.63594 2.6875,6.3425l51.6,51.6c1.70656,1.70656 3.92375,2.6875 6.3425,2.6875c2.41875,0 4.74344,-0.98094 6.45,-2.6875l67.51,-67.51c3.03688,-3.03687 3.02344,-7.28312 3.01,-11.395v-4.4075c0,-8.77469 0,-31.90062 0,-38.8075v-1.935c0,-5.99312 -4.86437,-10.8575 -10.8575,-10.8575h-45.15c-1.03469,0 -2.09625,-0.04031 -3.1175,0zM137.6,48.16c1.76031,0 3.49375,0.69875 4.8375,2.0425c2.6875,2.6875 2.6875,6.9875 0,9.675c-2.6875,2.6875 -6.9875,2.6875 -9.675,0c-2.6875,-2.6875 -2.6875,-6.9875 0,-9.675c1.34375,-1.34375 3.07719,-2.0425 4.8375,-2.0425z"></path></g></g></svg></p>
                        <%
                            System.out.println("tags--->"+    (highlights.getTags().size()>0  )  );
                            if( highlights.getTags().size()  >0 )
                            {

                                for(int j=0;j<highlights.getTags().size();j++)
                                {
                                    System.out.println(highlights.getTags().get(j) );
                        %>
                        <span><%=highlights.getTags().get(j).trim()%>  </span>
                        <%
                                }
                            }
                        %>
                        <i></i>

                    </div>
                </div><br/>
                <h6>By <a href="#" onclick="listUser('<%=highlights.getCreated_user()%>')"> <%=highlights.getUsername()%></a> <%= highlights.getHours() %> hours ago</h6>

                <!--<a href="#"><span>#NarendraModi</span></a>
                <a href="#"><span>#MudraBank</span></a>-->
                <div class="article-shadow"></div>
                <div class="article-action">
                    <div class="article-section-footer">
                        <div class="article-footer-icon-container-like" title="Like" id=like_<%=highlights.getId()%>  >
                            <img src="<%=request.getContextPath()%>/resources/img/like-black-new-1.png" alt="img" width="22" height="22">
                            <p class="like-count" id=like-count_<%=highlights.getId()%> > <%=highlights.getVotes()%> </p>
                            <%
                                String emailId=highlights.getLiked_user();
                                if(request.getSession().getAttribute("email").toString().length() <= 0 )
                                {

                            %>
                            <script>
                                document.getElementById("like_"+<%=highlights.getId()%>).removeAttribute("onclick");
                                //              document.getElementById("like_"+<%=highlights.getId()%>).style.pointerEvents="none";
                                //
                                console.log("inside like befor")
                                $("#like_"+<%=highlights.getId()%>).click(function()
                                {
                                    console.log("inside like")
                                    var modal = document.getElementById("modal-login");
                                    modal.style.display = "block";
                                })

                            </script>
                            <%
                                }

                                if(!highlights.getLiked_user().contains("0")){
                                    System.out.println("inside email----");
                            %>
                            <script>
                                document.getElementById("like_"+<%=highlights.getId()%>).firstElementChild.setAttribute("src", "<%=request.getContextPath()%>/resources/img/like-blue-new.png");
                                document.getElementById("like_"+<%=highlights.getId()%>). style.cursor="default";


                            </script>
                            <%
                                }
                            %>

                        </div>


                        <div class="article-footer-icon-container-delete" title="Delete"  onclick="deleteLink(<%=highlights.getId()%>)" id=delete-link_<%=highlights.getId()%>>
                            <img src="<%=request.getContextPath()%>/resources/img/trash-black-new-1.png" alt="img" width="24" height="24" class="ml-1 mt-1">

                            <%

                                if(request.getSession().getAttribute("email").toString().length() <= 0 )
                                {

                            %>
                            <script>
                                document.getElementById("delete-link_"+<%=highlights.getId()%>).removeAttribute("onclick");
                                // document.getElementById("delete-link_"+<%=highlights.getId()%>).style.pointerEvents="none";
                                console.log("inside delete before ")
                                $("#delete-link_"+<%=highlights.getId()%>).click(function()
                                {
                                    var modal = document.getElementById("modal-login");
                                    modal.style.display = "block";

                                });
                            </script>
                            <%
                                }
                            %>

                        </div>

                        <div class="article-footer-icon-container-comment" title="Comment" onclick="comments(<%=highlights.getId()%>)">
                            <img src="<%=request.getContextPath()%>/resources/img/comment-new-icon.png" alt="img" width="24" height="24" class="ml-1 mt-1">
                        </div>
                        <div class="article-footer-icon-container-email" title="Email" onclick="emailShare('<%=highlights.getImage_url()%>','<%=highlights.getUrl()%>' , '<%=highlights.getTitle().replaceAll("'"," ") %>')">
                            <img src="<%=request.getContextPath()%>/resources/img/email-icon-black.png" alt="img" width="23" height="23" class="ml-1 mt-1">
                        </div>
                        <div class="article-footer-icon-container-tag" title="Add Tags" id="add-tags">
                            <img  src="<%=request.getContextPath()%>/resources/img/tags-new.png" alt="img" width="23" height="23" class="ml-1 mt-1" data-toggle="modal" data-target=#myModal_<%=highlights.getId()%>>
                        </div>
                        <%
                            System.out.println("length--->"+highlights.getCopy_url().toString());
                            System.out.println("length--->"+highlights.getCopy_url().toString().trim().length());
                            if(highlights.getCopy_url().toString().length()>1)
                            {
                        %>
                        <div class="article-footer-icon-container-url" title="Open URL" onclick="openUrl('<%=highlights.getCopy_url()%>')">
                            <img src="<%=request.getContextPath()%>/resources/img/url-new.png" alt="img" width="17" height="17" class="ml-1 mt-1">
                        </div>
                        <%
                            }
                        %>


                        <!-- Modal -->
                        <div class="modal fade" id=myModal_<%=highlights.getId()%> role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="card-line-number-modal"></div>
                                    <!--<div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>-->
                                    <div class="modal-body ml-2">
                                        <input type="text" placeholder="Enter your tags" id=tags-input_<%=highlights.getId()%> autofocus>
                                        <button type="button" value="Submit" data-dismiss="modal" class="modal-submit">Submit</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <script>
                            $(function(){ // DOM ready

                                // ::: TAGS BOX

                                var t='#tags-input_<%=highlights.getId()%>' ;
                                var t1='#tags_<%=highlights.getId()%>>i' ;
                                var id=<%=highlights.getId()%>;
                                $( t).on({

                                    focusout : function() {
                                        console.log("this",this);
                                        console.log($(this).parent().parent().parent().parent().attr('id') )
                                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters

                                        if(txt) {

                                            var t=0;
                                            $('div#tags_<%=highlights.getId()%>  span').each(function(){

                                                console.log($(this).text());
                                                console.log($(this).text().trim() =="jsoup");
                                                console.log($(this).text() === "jsoup");
                                                if($(this).text().trim().toUpperCase()==txt.toUpperCase())
                                                {
                                                    console.log("already exist");
                                                    t=1;
                                                }

                                            });
                                            if(t==0)
                                            {
                                                $("<span/>", {text: txt.toLowerCase(), insertBefore: t1});

                                                $.ajax({type:"GET",
                                                    url: "/99HighlightsSaveTags?id=" +id + "&tags=" + txt ,
                                                    success: function(result){
                                                    }})
                                            }

                                        }
                                        this.value = "";

                                    },
                                    keyup : function(ev) {
                                        // if: comma|enter (delimit more keyCodes with | pipe)
                                        if(/(188|13)/.test(ev.which)) $(this).focusout();
                                    }
                                });
                                $(this).on('click', 'span', function(e) {
                                    // $(this).remove();
                                    e.stopImmediatePropagation();
                                    e.preventDefault();
                                    console.log("this-->",this);

                                    console.log("this.",this.innerHTML);

                                    //window.open("http://localhost:8080/tagsSearch?word="+this.innerHTML ,"_self")
                                    window.open("http://www.99highlights.com/tagsSearch?word="+this.innerHTML ,"_self")


                                });

                            });

                        </script>
                    </div>
                </div>
            </div>
            <div class="box-shadow"></div>
        </div>

        <%
                }
            }
        %>

        <div id="loading" style="display: none">
            <img src="<%=request.getContextPath()%>/resources/img/loader-new.gif"  alt="Loading…" class="loading-img"/>
        </div>

    </div>
</div>



<script>


    $(document).ready(function() {
        $(".main").each(function(index, el) { // `elements` is the common class for all elements on which this functionality is to be done
            if (index >= 6) { // maxLimitToShow - number of elements to show in the beginning
                $(el).addClass("hidden"); // for extra elements
            }
        });
    });

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {  // checks if scroll reached at bottom
            var elements = $(".main.hidden"); // fetch all hidden elements
            if(elements.length  < 0) { document.getElementById("loading").style.display="none";}
            if(elements .length >0 ){document.getElementById("loading").style.display="block";}

            setTimeout(function () {
                document.getElementById("loading").style.display="none";
                elements.each(function (index, el) {
                    if (index < 3) { // singleRowLimit  - number of elements to show in one scroll
                        $(el).removeClass('hidden').show();
                    }
                });
            },2000)

        }
    });

</script>


<script>

    function listUser(emailId)
    {
        var decoded_email=window.btoa(emailId)
        console.log("deco--->",decoded_email);
        window.location="/listUserStories?id="+decoded_email;
    }
</script>

<script>

    function openUrl(url )
    {

        window.open(url,"_blank");
    }

</script>

<script>

    function emailShare(imgUrl,url,title){
        var email='<%=request.getSession().getAttribute("email")%>';
        var email_send = prompt("Enter E-mail id ", email);

        if (email_send == null || email_send == "") {

        }
        else
        {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                //url: "http://localhost:8080/SendEmail?toMail="+email_send +"&url="+ encodeURIComponent(url) +  "&fromMail="+email +"&title="+encodeURIComponent(title)+"&imgUrl="+encodeURIComponent(imgUrl),
                url: "http://www.99highlights.com/SendEmail?toMail=" + email_send + "&url=" + encodeURIComponent(url) +"&fromMail=" + email + "&title=" + encodeURIComponent(title)+"&imgUrl="+encodeURIComponent(imgUrl),
                success: function (data) {

                    clearSelection();
                }
            });
        }

    }

</script>

<script>


/*
    function like(id)
    {
        console.log("here"+id);

        document.getElementById("like_"+id).removeAttribute("onclick");



        var email = '<%=request.getSession().getAttribute("email")%>';
        console.log("user email"+email);

        $.ajax({type:"GET",
            url: "/99HighlightsSaveLikes?id=" +id + "&emailId=" + email ,
            success: function(result){
                console.log("result"+result);
                document.getElementById("like-count_"+id).innerHTML=result;
                document.getElementById("like_"+id).firstElementChild.setAttribute("src","<%=request.getContextPath()%>/resources/img/like-blue-new.png");
                document.getElementById("like_"+id).setAttribute( "onClick", "dislike("+id+" )" );
                //document.getElementById("like_"+id).style="cursor:none";
                //document.getElementById("like_"+id).style.pointerEvents="none";




            }})
    }

    function dislike(id)
    {


        document.getElementById("like_"+id).removeAttribute("onclick");



        var email = '<%=request.getSession().getAttribute("email")%>';
        console.log("user email"+email);

        $.ajax({type:"GET",
            url: "/99HighlightsSaveDisLikes?id=" +id + "&emailId=" + email ,
            success: function(result){
                console.log("result"+result);
                document.getElementById("like-count_"+id).innerHTML=result;
                document.getElementById("like_"+id).firstElementChild.setAttribute("src","<%=request.getContextPath()%>/resources/img/like-black-new-1.png");
                document.getElementById("like_"+id).setAttribute( "onClick", "like("+id+" )" );
                //document.getElementById("like_"+id).style="cursor:none";
                //document.getElementById("like_"+id).style.pointerEvents="none";




            }})
    }
*/
</script>


<script>
    function logout()
    {
        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.cookie = "GL=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.getElementById("myBtn").style.display="block";
        document.getElementById("login").style.display="none";
        // window.open("http://localhost:8080/logout","_self");
        window.open("http://www.99highlights.com/logout","_self");

    }

</script>

<script>

    function login()
    {

        //window.open("https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://localhost:8080/oauth2callback&response_type=code&client_id=420424849668-t6e1k7em1erhpi4rnvf705g5l4gjifj7.apps.googleusercontent.com&approval_prompt=force","_self" );
        window.open("https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://www.99highlights.com/oauth2callback&response_type=code&client_id=1052984239121-vt400kdkvaedlqv7s5o4aqia0cdgprpu.apps.googleusercontent.com&approval_prompt=force","_self" );
    }
</script>


<script>
    function deleteLink(id)
    {

        var email='<%=request.getSession().getAttribute("email")%>'
        $.ajax({type:"GET",
            url: "/99HighlightsDelete?id=" +id + "&email=" + email ,
            success: function(result){
                console.log(result)
                if(result ==3)
                {
                    document.getElementById("delete-link_"+id).style.display="none";
                    document.getElementById("card_"+id).style.display="none";
                }
            }})

    }
</script>

<script>

    function comments(id) {
        //window.open("http://localhost:8080/comments?id="+id,"_self");
        window.open("http://www.99highlights.com/comments?id="+id,"_self");
    }
</script>

<script>
    // Get the modal
    console.log('inside of modal-login');
    var modal = document.getElementById("modal-login");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementById("close-google");

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        console.log('inside of btn');
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>


<script>
    // Get the modal

    document.getElementById("add-url").addEventListener("click",function () {


        var emailId='<%=request.getSession().getAttribute("email")%>' ;

        console.log("emailId",emailId);
        console.log(emailId.toString().length);

        if(emailId.length>0  ) {
            console.log('inside of modal add url');
            var modal1 = document.getElementById("modal-add-url");

            // Get the button that opens the modal
            var btn = document.getElementById("add-url");

            var submit_btn = document.getElementById("submit-url");
            // Get the <span> element that closes the modal
            var span = document.getElementById("close-url");

            modal1.style.display = "block";

            // When the user clicks the button, open the modal
            btn.onclick = function () {
                console.log('inside of btn');
                modal1.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal1.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal1.style.display = "none";
                }
            }

            submit_btn.onclick = function () {
                console.log('iniside of submit');
                modal1.style.display = "none";

                var url = document.getElementById("URL").value;
                var tags = document.getElementById("tags-input-url").value;

                console.log("url--->", url);
                console.log("tags--->", tags);
                window.location.href = '/AddLink?url=' + url + '&tags=' + tags;


            }
        }
        if(emailId <=0 )
        {
            var modal = document.getElementById("modal-login");
            modal.style.display = "block";
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
    })



</script>

<script>
    $(document).ready(function(){

        var chrome_id;

        var cookiestring  = document.cookie;
        var cookiearray = cookiestring.split(';');
//        console.log(cookiearray);
        for(var i =0 ; i < cookiearray.length ; ++i){

            if(cookiearray[i].trim().match('^'+"LUTWChromeId"+'=')){
                console.log( cookiearray[i].replace("LUTWChromeId=",'').trim());
                chrome_id=cookiearray[i].replace("LUTWChromeId=",'').trim();
            }



        }

        if(chrome_id!="")
        {
            document.getElementById("chrome-id").style.display="none"
        }




    });
</script>
<script>
    // Get the modal
    console.log('inside of modal add url');
    var modal1 = document.getElementById("modal-about");

    // Get the button that opens the modal
    var btn = document.getElementById("about");

    var submit_btn = document.getElementById("submit-tags");
    // Get the <span> element that closes the modal
    var span = document.getElementById("close-url");

    var close_tabs = document.getElementById('close-about');

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        console.log('inside of btn');
        modal1.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal1.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal1.style.display = "none";
        }
    }

    submit_btn.onclick = function(){
        console.log('iniside of submit');
        modal1.style.display="none";
    }
    close_tabs.onclick = function(){
        console.log('iniside of submit');
        modal1.style.display="none";
    }
</script>
<script>
    $(document).ready(function() {
        console.log('inside of jquery');
        /*var myVar;
         myVar = setTimeout(showPage, 3000);
         function showPage() {
         console.log('inside of settimeout');
         document.getElementById("cc").style.display = "none";
         document.getElementById("dd").style.display = "block";
         }*/
        function explode(){
            console.log('inside of explode');
            document.getElementById("content-loader").style.display = "none";
            document.getElementById("main-content").style.display = "block";
        }
        setTimeout(explode, 1000);
    });

</script>

</html>