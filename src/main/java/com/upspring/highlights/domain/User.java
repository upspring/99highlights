package com.upspring.highlights.domain;

/**
 * Created by Sathya on 15/06/20.
 */
public class User implements java.io.Serializable {

    private  String emailId;
    private String username;
    private String picture;
    private int id;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
