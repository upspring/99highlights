package com.upspring.highlights.domain;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sathya on 15/06/20.
 */
@Controller
public class Home implements java.io.Serializable {

        private int id;
        private String url;
        // private Date date_created;
        private int vote_up;
        private int vote_down;
        private int deleted;
        private int user_id;
        private String title;
        private  String data;
        private String className;
        private String date_created;
        private Date date;
        private int likes;
        private String username;
        private String email;
        private  String searchWord;
        private String description;
        private  int liked;
        private  int publish;
        private int count;
        private String highlightId;
        private String pageLogo;
        private String color;
        private int favourite;
        private int bookmarked;
        private  int highlightProjectId;
        private String timestamp;
        private List<Home> comments;
        private String commentData;
        private  String commentEmailId;
        private String commentDate;
        private String defaultUsername;
        private String commentUserPic;
        private String hours;
        private String image_url;
        private int votes;
        private  List<String> tags;
        private String copy_url;
        private String user_pic;
        private String liked_user;
        private  String created_user;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setDate_created(String date_created) {
            this.date_created = date_created;
        }

        public String getDate_created() {
            return date_created;
        }



        public int getDeleted() {
            return deleted;
        }

        public void setDeleted(int deleted) {
            this.deleted = deleted;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getVote_up() {
            return vote_up;
        }

        public void setVote_up(int vote_up) {
            this.vote_up = vote_up;
        }

        public int getVote_down() {
            return vote_down;
        }

        public void setVote_down(int vote_down) {
            this.vote_down = vote_down;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }


        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSearchWord() {
            return searchWord;
        }

        public void setSearchWord(String searchWord) {
            this.searchWord = searchWord;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }



        public int getLiked() {
            return liked;
        }

        public void setLiked(int liked) {
            this.liked = liked;
        }

        public int getPublish() {
            return publish;
        }

        public void setPublish(int publish) {
            this.publish = publish;
        }


        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getHighlightId() {
            return highlightId;
        }

        public void setHighlightId(String highlightId) {
            this.highlightId = highlightId;
        }

        public String getPageLogo() {
            return pageLogo;
        }

        public void setPageLogo(String pageLogo) {
            this.pageLogo = pageLogo;
        }


        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getFavourite() {
            return favourite;
        }

        public void setFavourite(int favourite) {
            this.favourite = favourite;
        }

        public int getBookmarked() {
            return bookmarked;
        }

        public void setBookmarked(int bookmarked) {
            this.bookmarked = bookmarked;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getHighlightProjectId() {
            return highlightProjectId;
        }

        public void setHighlightProjectId(int highlightProjectId) {
            this.highlightProjectId = highlightProjectId;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }



        public String getCommentData() {
            return commentData;
        }

        public void setCommentData(String commentData) {
            this.commentData = commentData;
        }

        public String getCommentEmailId() {
            return commentEmailId;
        }

        public void setCommentEmailId(String commentEmailId) {
            this.commentEmailId = commentEmailId;
        }

        public List<Home> getComments() {
            return comments;
        }

        public void setComments(List<Home> comments) {
            this.comments = comments;
        }

        public String getCommentDate() {
            return commentDate;
        }

        public void setCommentDate(String commentDate) {
            this.commentDate = commentDate;
        }

        public String getDefaultUsername() {
            return defaultUsername;
        }

        public void setDefaultUsername(String defaultUsername) {
            this.defaultUsername = defaultUsername;
        }

        public String getCommentUserPic() {
            return commentUserPic;
        }

        public void setCommentUserPic(String commentUserPic) {
            this.commentUserPic = commentUserPic;
        }

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }


    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }



    public String getCopy_url() {
        return copy_url;
    }

    public void setCopy_url(String copy_url) {
        this.copy_url = copy_url;
    }

    public  List<String> getTags() {
        return tags;
    }

    public void setTags( List<String> tags) {
        this.tags = tags;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }

    public String getLiked_user() {
        return liked_user;
    }

    public void setLiked_user(String liked_user) {
        this.liked_user = liked_user;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }
}
