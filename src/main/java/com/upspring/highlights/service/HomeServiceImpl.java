package com.upspring.highlights.service;

import com.upspring.highlights.dao.HomeDAO;
import com.upspring.highlights.domain.Home;
import com.upspring.highlights.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Sathya on 15/06/20.
 */
@Service
public class HomeServiceImpl  implements  HomeService{
    @Autowired
    HomeDAO homeDAO;

    public  void sendEmail(String toMail, String pageUrl, String fromMail, String decodeTitle,String imgUrl)
    {
        try {
            String userName= "sathiya";
            String apiKey= "850c991d-fe69-44ee-bccb-94fc8edb0a1a";
            //"a526a16a-909a-48d9-bc62-91abc034999b";
            String fromName="LightUpTheWeb";
            String from="info@upspring.it";
            String subject="New Highlight from"+" "+fromMail;
            String isTransactional="false";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            System.out.println("current date is--->"+dateFormat.format(date));

            String ems="";




             String body = String.join( " <table class='entire-page' style=' background: #C7C7C7; width: 100%; padding: 20px 0; font-family: 'Lucida Grande', 'Lucida Sans Unicode', Verdana, sans-serif; line-height: 1.5; '>",
                  "<tr>",
                      "<td style=' font-size: 13px; color: #878787; background-color: #878787 !important'>",
                        "<table class='email-body' style='box-shadow: 0 0.2rem .5rem rgba(0,0,0, 0.12) !important; background-color: white !important; max-width: 600px; min-width: 320px; margin: 0 !important;  border-collapse: collapse; '>",
                          "<tr style='padding: 0; margin: 0;'>",
                            "<td class='email-header'' style='background: black; padding: 15px 15px 05px 15px;'>",
                              "<a href='#' style='color: black; font-weight: bold; text-decoration: none;'>",
                                "<img src='http://www.99highlights.com/resources/img/logo-email.png' alt='CodePen News' class='header-img' style='width: 40%; height: 18%; display: block; margin-left: auto; margin-right: auto; width: 50%;'>",
                              "</a>",
                            "</td>",
                          "</tr>",
                          "<tr style='padding: 0; margin: 0;'>",
                            "<td class='news-section' style='padding: 20px 30px;'>",
                            "<a href="+pageUrl+" style='color:black; font-weight: bold; text-decoration: none;'> <h1 style='font-size: 20px;'>"+ decodeTitle+" </h1></a>",
                              "<img src="+ imgUrl+ "  style='width: 550px; height: 350px; border: 1px solid #222;'>",
                            "</td>",
                          "</tr>",
                          "<tr style='padding: 0; margin: 0;'>",
                           "<td class='footer' style='background: #000;  padding: 10px; font-size: 12px; border: 1px solid #ddd; font-family: sans-serif; text-align:center'>",
                                "<p style='text-align: center !important; padding:0; margin:0; color: #fff; width: 620px !important'> ® 99highlights, Copyright 2020</p>",
                            "</td>",
                          "</tr>",
                        "</table>",
                      "</td>",
                  "</tr>",
             "</table> "
            );


/*String body=String.join("<div style='border: 1px solid #ddd; width: 50%; margin: 0 auto; padding: 0; background-color: transparent'>" ,
        "           <div style='background: black; padding: 15px 15px 05px 15px;'>" ,
        "                <a href='#' style='color: black; font-weight: bold; text-decoration: none;'>" ,
        "                    <img src='http://www.99highlights.com/resources/img/logo-email.png' alt='CodePen News' class='header-img' style='width: 40%; height: 15%; display: block; margin-left: auto; margin-right: auto; '>" ,
        "                </a>" ,
        "            </div>" ,
        "            <div style='padding: 0; margin: 0;'>" ,
        "                <div style='padding: 20px 30px;'>" ,
        "                    <a href= "+pageUrl+ " style='color:black; font-weight: bold; text-decoration: none;'> <h1 style='font-size: 20px; font-family: sans-serif; line-height: 32px;'>  "+decodeTitle +"    </h1></a>" ,
        "                    <!--<img src=\"+ imgUrl+ \"  style='width: 550px; height: 350px; border: 1px solid #222;'>-->" ,
        "                    <img src= "+imgUrl+ "  style='width: 100%; height: 350px; border: 1px solid #222;'>" ,
        "                </div>" ,
        "            </div>" ,
        "            <div style='padding: 0; margin: 0'>" ,
        "                <div style='background: #eee; padding: 10px; font-size: 12px; width: 600px; font-family: sans-serif; width: 97.2%'>" ,
        "                    <p style='text-align: center; padding:0; margin:0'> ® 99highlights.com, Copyright 2020</p>" ,
        "                </div>" ,
        "            </div>" ,
        "        </div>");
*/




            System.out.println("Elastic email");
            String encoding = "UTF-8";

            String data = "apikey=" + URLEncoder.encode(apiKey, encoding);
            data += "&from=" + URLEncoder.encode(from, encoding);
            data += "&fromName=" + URLEncoder.encode(fromName, encoding);
            data += "&subject=" + URLEncoder.encode(subject, encoding);
            data += "&bodyHtml=" + URLEncoder.encode(body, encoding);
            data += "&to=" + URLEncoder.encode(toMail, encoding);
            data += "&isTransactional=" + URLEncoder.encode(isTransactional, encoding);

            URL url = new URL("https://api.elasticemail.com/v2/email/send");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String result = rd.readLine();
            wr.close();
            rd.close();

            System.out.println(result);
        }catch (Exception e) {

            e.printStackTrace();
        }
    }





    public List<Home> get99Highlights(String emailId) { return  homeDAO.get99Highlights(emailId);}
    public List<Home> getNew99Highlights(String emailId){return  homeDAO.getNew99Highlights(emailId);}
    public  List<Home> getMyStories(String emailId){return  homeDAO.getMyStories(emailId);}
    public  List<Home> getLikedStories(String emailId) {return  homeDAO.getLikedStories(emailId);}

    public  List<Home> listUserStories(String emailId,String currentUser) { return  homeDAO.listUserStories(emailId,currentUser);}

    public  int insert99PageLikes(int id,String emailId){return  homeDAO.insert99PageLikes(id,emailId); }
    public  int insert99PageDisLikes(int id,String emailId){return  homeDAO.insert99PageDisLikes(id,emailId); }
    public  void save99HighlightsTags(int id,String tags) {homeDAO.save99HighlightsTags(id,tags);}
    public User getGmailLoginUserDetail(String emailId){
        return  homeDAO.getGmailLoginUserDetail (emailId);
    }

    public void updateUserPicture(String emailId,String picture){
        homeDAO.updateUserPicture(emailId,picture);
    }
    public boolean addGmailLoginUser(User newUser)
    {
        return homeDAO.addNewUser(newUser);
    }
    public void addLink(String url,String title,String userAdded,String imageUrl,String tags,String emailId)
    {
        homeDAO.addLink(url,title,userAdded,imageUrl,tags,emailId);
    }

    public  List<Home> getTagsLink (String word,String emailId){
        return  homeDAO.getTagsLink(word,emailId);
    }
    public int deleteLink(String url,String email)
    {
        return  homeDAO.deleteLink(url,email);
    }
    public  List<Home> getComments(int id,String emailId) {return  homeDAO.getComments(id,emailId);}

    public String  saveComments(String commentId,String comments) {return homeDAO.saveComments(commentId,comments);}

    public  String getCommentsUpdate(String commentId) {return  homeDAO.getCommentsUpdate(commentId);}



    public  String updateComments (String commentId,String comments) {return  homeDAO.updateComments(commentId,comments);}

    public List<String>  getCommentsforPage(String id) { return  homeDAO.getCommentsforPage(id);}
}
