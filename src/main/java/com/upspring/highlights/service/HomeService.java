package com.upspring.highlights.service;

import com.upspring.highlights.domain.Home;
import com.upspring.highlights.domain.User;

import java.util.List;

/**
 * Created by Sathya on 15/06/20.
 */
public interface HomeService {


    public  void sendEmail(String toMail, String pageUrl, String fromMail, String decodeTitle,String imgUrl);
    public List<Home> get99Highlights(String emailId);
    public List<Home> getNew99Highlights(String emailId);

    public  int insert99PageLikes(int id,String emailId);
    public  int insert99PageDisLikes(int id,String emailId);

    public  void save99HighlightsTags(int id,String tags);
    public User getGmailLoginUserDetail(String emailId);
    public void updateUserPicture(String emailId,String picture);
    public boolean addGmailLoginUser(User newUser);
    public void addLink(String url,String title,String userAdded,String image_url,String tags,String emailId);
    public  List<Home> getTagsLink (String word,String emailId);
    public int deleteLink(String url,String email);
    public  List<Home> getComments(int id,String emailId);
    public  List<Home> getMyStories(String emailId);
    public  List<Home> getLikedStories(String emailId);
    public  List<Home> listUserStories(String emailId,String currentUser);

    public String  saveComments(String commentId,String comments);

    public  String updateComments (String commentId,String comments);
    public List<String>  getCommentsforPage(String id);
    public  String getCommentsUpdate(String commentId);
}
