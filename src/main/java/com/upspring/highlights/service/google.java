package com.upspring.highlights.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/oauth2callback")
public class google extends HttpServlet
{



    private static final long serialVersionUID = 1L;
    static String className = "com.upspring.highlight.service.google";

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            System.out.println("inside of oauth2 call back.......");

            String code = request.getParameter("code");



            String url = "https://accounts.google.com/o/oauth2/token";
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);


           // 99Highlights credentials

              nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
              nameValuePairs.add(new BasicNameValuePair("client_id","1052984239121-vt400kdkvaedlqv7s5o4aqia0cdgprpu.apps.googleusercontent.com"));
              nameValuePairs.add(new BasicNameValuePair("client_secret", "LJBM_mNTvGkH9wx7nEu9f-fV"));
              nameValuePairs.add(new BasicNameValuePair("redirect_uri", "http://www.99highlights.com/oauth2callback"));
              nameValuePairs.add(new BasicNameValuePair("code", code));


            //Localhost Credentials
/*              nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));

            nameValuePairs.add(new BasicNameValuePair("client_id","420424849668-t6e1k7em1erhpi4rnvf705g5l4gjifj7.apps.googleusercontent.com"));
            nameValuePairs.add(new BasicNameValuePair("client_secret", "1QCH7VLDs3FWHveDDPDBaKLI"));
            nameValuePairs.add(new BasicNameValuePair("redirect_uri", "http://localhost:8080/oauth2callback"));
            nameValuePairs.add(new BasicNameValuePair("code", code));
*/


            //424828955243-mr070on0hf7amcvrir0aut2okddctvg4.apps.googleusercontent.com
            //OidrQ3gM_t4OFN_9DqkZ17Rq
            //https://www.lightuptheweb.net/oauth2callback

            HttpPost post = new HttpPost(url);
            HttpClient client = new DefaultHttpClient();

            // add header
            post.setHeader("Host", "accounts.google.com");
            post.setHeader("Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            post.setHeader("Accept-Language", "en-US,en;q=0.5");
            post.setHeader("Connection", "keep-alive");
            post.setHeader("Referer", "https://accounts.google.com/o/oauth2/token");
            post.setHeader("Content-Type", "application/x-www-form-urlencoded");

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response2 = client.execute(post);

            int responseCode = response2.getStatusLine().getStatusCode();

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response2.getEntity().getContent()));

            String result="";
            String line = "";
            while ((line = rd.readLine()) != null) {
                result+=line;
            }


            System.out.println("Input Reader--->"+result);
            //get Access Token
            JsonObject json = (JsonObject)new JsonParser().parse(result);
            String access_token = json.get("access_token").getAsString();
            System.out.println("Access Token-->"+access_token);


            GoogleCredential cr=new GoogleCredential().setAccessToken(access_token);

            System.out.println("cr"+cr);
            Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), cr).setApplicationName(
                    "Oauth2").build();
            System.out.println("userInfo<<------>>"+oauth2);
            Userinfoplus userinfo = oauth2.userinfo().get().execute();
            System.out.println("userInfo<<------>>"+userinfo);
            userinfo.toPrettyString();
            System.out.println("userInfo<<------>>"+userinfo.getEmail());
            String email=userinfo.getEmail();
            String user=userinfo.getGivenName();
            String picture=userinfo.getPicture();

            System.out.println("working......"+user);
            String userName=URLEncoder.encode(user,"UTF8");

            System.out.println("username<<------>>"+userName);
            System.out.println("userInfo<<------>>"+userinfo.getEmail());

            System.out.println("userInfo Encoded Username<<------>>"+userName);


           String encodeEmail= new String(java.util.Base64.getEncoder().encode(email.getBytes()));

            System.out.println("encodeEmail"+encodeEmail);

            Cookie ck=new Cookie("GL",encodeEmail);//creating cookie object
            ck.setMaxAge(60*60*24*365);
            response.addCookie(ck);//adding cookie in the response

            Cookie ck1=new Cookie("username",user);//creating cookie object
            ck1.setMaxAge(60*60*24*365);
            response.addCookie(ck1);

            Cookie ck2=new Cookie("userPic",picture);//creating cookie object
            ck2.setMaxAge(60*60*24*365);
            response.addCookie(ck2);









            //Update Tp lightuptheweb

            URL userUrl = new URL(
                     //"http://localhost:8080/SocialLogin?email=" + email+"&userName="+userName+"&network=GL&picture="+picture
                     "http://www.99highlights.com/SocialLogin?email=" + email+"&userName="+userName+"&network=GL&picture="+picture
            );

            URLConnection  urlConn = userUrl.openConnection();
            String userDeatil="";
            rd = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                userDeatil += line;
            }

            System.out.println("Login User details--->"+userDeatil);
           /* if(userDeatil!=null&&userDeatil!="")
            {

                //String colorValue= userDeatil.replaceAll("^\"|\"$", "");
                System.out.println("color value is"+userDeatil);


                Cookie ck2=new Cookie("COLOR",userDeatil);//creating cookie object
                ck2.setMaxAge(60*60*24*365);
                //response.addCookie(ck2);//adding cookie in the response

            }
*/

            response.sendRedirect("http://www.99highlights.com");
            rd.close();


        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (ProtocolException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {


        System.out.println("do post");
    }



}
