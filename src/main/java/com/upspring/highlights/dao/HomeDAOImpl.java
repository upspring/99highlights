package com.upspring.highlights.dao;

import com.upspring.highlights.domain.Home;
import com.upspring.highlights.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Sathya on 15/06/20.
 */
@Repository
public class HomeDAOImpl implements  HomeDAO{


    @Autowired(required = false)
    private JdbcTemplate jdbcTemplateObj;

    @Autowired(required = false)
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }



    public List<Home> getNew99Highlights(String emailId)
    {

        String getHighlightDataSql="";
        if(emailId.length()<0) {
            getHighlightDataSql = "select (0) as liked_user,u.email,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3  group by a.url order by a.date_created desc; ";
        }
        else
        {
            getHighlightDataSql = " select (select count(*) from 99_highlights_likes where page_id=a.id and user_id=(select id from user where email=:emailId  )) as liked_user, u.email,  u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id   where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3    group by a.url order by a.date_created desc;  ";
        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("emailId",emailId);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                System.out.println("title---->"+rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                highlight.setCreated_user(rs.getString("email"));
                List<String> arr= new ArrayList<>();
                if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                {
                    String[] arr1=rs.getString("tags").split(",");
                    arr= Arrays.asList(arr1);
                    highlight.setTags(arr);
                }
                else
                {

                    highlight.setTags(arr );
                }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());


        return highlightDataList;

    }

    public List<Home> getMyStories(String emailId)
    {

        String getHighlightDataSql="";
        if(emailId.length()<0) {
            getHighlightDataSql = "select (0) as liked_user,u.email,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3  group by a.url order by a.date_created desc; ";
        }
        else
        {
            getHighlightDataSql = "  select (select count(*) from 99_highlights_likes where page_id=a.id and user_id=(select id from user where email=:emailId  )) as liked_user, u.email,  u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id   where a.deleted<3   and a.user_id=(select id from user where email=:emailId)   order by date_created desc  ; ";
        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("emailId",emailId);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                System.out.println("title---->"+rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setCreated_user(rs.getString("email"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                List<String> arr= new ArrayList<>();
                if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                {
                    String[] arr1=rs.getString("tags").split(",");
                    arr= Arrays.asList(arr1);
                    highlight.setTags(arr);
                }
                else
                {

                    highlight.setTags(arr );
                }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());


        return highlightDataList;

    }

    public  List<Home> listUserStories(String emailId,String currentUser)
    {

        String getHighlightDataSql="";
        if(emailId.length()<0) {
            getHighlightDataSql = "select (0) as liked_user, u.email,  u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id   where a.deleted<3   and a.user_id=(select id from user where email=:emailId)   order by date_created desc  ;  ";
        }
        else
        {
            getHighlightDataSql = "  select (select count(*) from 99_highlights_likes where page_id=a.id and user_id=(select id from user where email=:currentUser  )) as liked_user, u.email,  u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id   where a.deleted<3   and a.user_id=(select id from user where email=:emailId)   order by date_created desc  ; ";
        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("emailId",emailId);
        parameters.put("currentUser",currentUser);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                System.out.println("title---->"+rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setCreated_user(rs.getString("email"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                List<String> arr= new ArrayList<>();
                if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                {
                    String[] arr1=rs.getString("tags").split(",");
                    arr= Arrays.asList(arr1);
                    highlight.setTags(arr);
                }
                else
                {

                    highlight.setTags(arr );
                }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());
return highlightDataList;

    }


    public List<Home> getLikedStories(String emailId)
    {

        String getHighlightDataSql="";
        if(emailId.length()<0) {
            getHighlightDataSql = "select (0) as liked_user,u.email,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3  group by a.url order by a.date_created desc; ";
        }
        else
        {
            getHighlightDataSql = "  select (select count(*) from 99_highlights_likes where page_id=a.id and user_id=(select id from user where email= :emailId  )) as liked_user, u.email,  u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id left join 99_highlights_likes ls on ls.page_id=a.id    where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3  and ls.user_id= (select id from user where email=:emailId) and ls.deleted=0 group by a.url order by a.date_created desc; ";
        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("emailId",emailId);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                System.out.println("title---->"+rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                highlight.setCreated_user(rs.getString("email"));
                List<String> arr= new ArrayList<>();
                if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                {
                    String[] arr1=rs.getString("tags").split(",");
                    arr= Arrays.asList(arr1);
                    highlight.setTags(arr);
                }
                else
                {

                    highlight.setTags(arr );
                }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());


        return highlightDataList;

    }


    public List<Home> get99Highlights( String emailId)
    {

        String getHighlightDataSql="";
        if(emailId.length()<0)
        getHighlightDataSql="select (0) as liked_user,u.user,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3    group by a.url order by v desc; ";
        else {
            getHighlightDataSql="select (select count(*) from 99_highlights_likes where page_id=a.id and deleted=0 and user_id=(select id from user where email=:emailId)) as liked_user,u.email,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3    group by a.url order by v desc; ";
        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("emailId",emailId);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                System.out.println("title---->"+rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setCreated_user(rs.getString("email"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                 List<String> arr= new ArrayList<>();
                    if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                    {
                              String[] arr1=rs.getString("tags").split(",");
                              arr= Arrays.asList(arr1);
                            highlight.setTags(arr);
                    }
                    else
                    {

                        highlight.setTags(arr );
                    }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());


          return highlightDataList;

    }

    public  int insert99PageLikes(int id,String emailId)
    {
        int result = 0;
        int voteUp = 0;



        String saveVoteUpSql = "update 99_highlights_page_link set votes=votes+1  where id=:id";


        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("id", id);

        result = jdbcTemplate.update(saveVoteUpSql, parameters);


        /**
         *    write code for who like the page 99_highlight_likes
         */

        try {
            String indertUserSql=" INSERT INTO 99_highlights_likes (page_id,user_id) values (:id , (select id from user where email=:email_id) )   ";
            HashMap<String,Object> parameters2 = new HashMap<String,Object>();
            parameters2.put("id",id);
            parameters2.put("email_id",emailId);

             jdbcTemplate.update(indertUserSql, parameters2);
        }
        catch(Exception e){
            e.printStackTrace();
        }




        String getVotesDownSql = "select votes from 99_highlights_page_link where id=:id";
        List<String> votes = null;
        Map<String, Object> parameters1 = new HashMap<String, Object>();
        parameters1.put("id", id);
        votes= jdbcTemplate.query(getVotesDownSql, parameters1, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {

                return rs.getString("votes");
            }
        });
        int numberOfVotesDown=Integer.parseInt(votes.get(0));
        return numberOfVotesDown;

    }


    public  int insert99PageDisLikes(int id,String emailId)
    {
        int result = 0;
        int voteUp = 0;

        System.out.println("emailId"+emailId);
        System.out.println("id-->"+id);


        String saveVoteUpSql = "update 99_highlights_page_link set votes=votes-1  where id=:id";


        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("id", id);

        result = jdbcTemplate.update(saveVoteUpSql, parameters);


        /**
         *    write code for who like the page 99_highlight_likes
         */

        try {
            String indertUserSql=" update 99_highlights_likes  set deleted=1 where page_id=:id  and user_id=(select id from user where email=:email_id)";
            HashMap<String,Object> parameters2 = new HashMap<String,Object>();
            parameters2.put("id",id);
            parameters2.put("email_id",emailId);

            jdbcTemplate.update(indertUserSql, parameters2);
        }
        catch(Exception e){
            e.printStackTrace();
        }




        String getVotesDownSql = "select votes from 99_highlights_page_link where id=:id";
        List<String> votes = null;
        Map<String, Object> parameters1 = new HashMap<String, Object>();
        parameters1.put("id", id);
        votes= jdbcTemplate.query(getVotesDownSql, parameters1, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {

                return rs.getString("votes");
            }
        });
        int numberOfVotesDown=Integer.parseInt(votes.get(0));
        return numberOfVotesDown;

    }



    public  void save99HighlightsTags(int id,String tags)
    {

        System.out.println("tags--->"+tags);

        String saveVoteUpSql = "update 99_highlights_page_link set tags= CONCAT( IF(IFNULL(tags,'')='','',CONCAT(tags,',')), :tags) where id=:id";


        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("id", id);
        parameters.put("tags",tags);

        jdbcTemplate.update(saveVoteUpSql, parameters);
    }

    public User getGmailLoginUserDetail(String emailId){
        User user = null;

        try {
            String getUserByUsernameSQL = "SELECT * from user where email=:email_id ";

            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("email_id", emailId);
            user = jdbcTemplate.queryForObject(getUserByUsernameSQL, parameters, new RowMapper<User>() {

                public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                    User user = new User();
                    user.setId(rs.getInt("id"));
                    user.setEmailId(rs.getString("email"));




                    return user;
                }

            });
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return user;
    }


    /** add the new gmail login user*/
    public boolean addNewUser(User newUser)
    {

        int result=0;
        try {
            String indertUserSql="INSERT INTO user(username,email,password,account_type,login_page,picture)"+
                    "VALUES(:username,:emailId,:password,:account_type,:login_page,:picture)";
            HashMap<String,Object> parameters = new HashMap<String,Object>();
            parameters.put("username",newUser.getUsername());
            parameters.put("emailId",newUser.getEmailId());
            parameters.put("account_type","GL");
            parameters.put("picture",newUser.getPicture());
            parameters.put("login_page","99Highlights");
            result=jdbcTemplate.update(indertUserSql, parameters);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return (result > 0) ? true : false;
    }



    /** update the user profile picture*/
    public void updateUserPicture(String emailId,String picture)
    {


        String setColorSql = "update user set picture=:picture WHERE email=:emailId and picture='null'";

        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("picture",picture );
        parameters.put("emailId", emailId);

        jdbcTemplate.update(setColorSql, parameters);

        //return result;

    }


    public void addLink(String url,String title,String userAdded,String imageUrl,String tags,String emailId)
    {


        int result=0;
        try {
            String indertUserSql=" INSERT INTO 99_highlights_page_link (url,title,added_link,image_url,tags,user_id,short_url)  values (:url ,:title,:user_added,:image_url,:tags,(select id from user where email=:email_id), :copy_url)  ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)";
            HashMap<String,Object> parameters = new HashMap<String,Object>();
            parameters.put("url",url);
            parameters.put("title",title);
            parameters.put("tags",tags.trim());
            parameters.put("image_url",imageUrl);
            parameters.put("user_added",userAdded);
            parameters.put("email_id",emailId);
            parameters.put("copy_url","");
            result=jdbcTemplate.update(indertUserSql, parameters);
        }
        catch(Exception e){
            e.printStackTrace();
        }




    }


    public  List<Home> getTagsLink (String word,String emailId){


        String getHighlightDataSql="";
        if(emailId.length()<0)
        getHighlightDataSql="select (0) as liked_user,u.email,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241 and a.deleted<3 and   a.tags like :word group by a.url order by v desc; ";
        else
        {

            getHighlightDataSql="select (select count(*) from 99_highlights_likes where page_id=a.id and deleted=0 and user_id=(select id from user where email=:emailId)) as liked_user,u.email,u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where timestampdiff(HOUR, a.date_created, now()) < 241 and a.deleted<3   and a.tags like :word group by a.url order by v desc; ";
        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("word","%"+word+"%");
        parameters.put("emailId",emailId);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                System.out.println("title---->"+rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                List<String> arr= new ArrayList<>();
                System.out.println("lenght---->"+rs.getString("tags").toString().length() );
                if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                {
                    String[] arr1=rs.getString("tags").split(",");
                    arr= Arrays.asList(arr1);
                    highlight.setTags(arr);
                }
                else
                {

                    highlight.setTags(arr );
                }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());


        return highlightDataList;

    }

    public int deleteLink(String id,String email){

        int result;

        String saveVoteUpSql = "update 99_highlights_page_link set deleted=deleted+1  where id=:id";
         HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("id", id);

        result = jdbcTemplate.update(saveVoteUpSql, parameters);


        try {
            String indertUserSql=" INSERT INTO 99_highlights_delete (page_id,user_id) values (:id , (select id from user where email=:email_id) )   ";
            HashMap<String,Object> parameters2 = new HashMap<String,Object>();
            parameters2.put("id",id);
            parameters2.put("email_id",email);

            jdbcTemplate.update(indertUserSql, parameters2);
        }
        catch(Exception e){
            e.printStackTrace();
        }




        String getVotesDownSql = "select deleted from 99_highlights_page_link where id=:id";
        List<String> votes = null;
        Map<String, Object> parameters1 = new HashMap<String, Object>();
        parameters1.put("id", id);
        votes= jdbcTemplate.query(getVotesDownSql, parameters1, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {

                return rs.getString("deleted");
            }
        });
        int numberOfVotesDown=Integer.parseInt(votes.get(0));
        return numberOfVotesDown;

    }

    public  List<Home> getComments(int id,String emailId)
    {

        String getHighlightDataSql="";
        if(emailId.length()<0) {
            getHighlightDataSql = "select (0) as liked_user,u.email,u.username,u.picture, a.*, timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where a.id=:id  and a.deleted<3   group by a.url; ";
        }
        else
        {
            getHighlightDataSql = "select (select count(*) from 99_highlights_likes where page_id=a.id and   deleted=0 and user_id=(select id from user where email=:emailId )) as liked_user,u.email,u.username,u.picture, a.*, timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id  left join 99_highlights_likes l on l.page_id=a.id where a.id=:id  and a.deleted<3   group by a.url; ";

        }

        List<Home> highlightDataList = null;
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("id",id);
        parameters.put("emailId",emailId);

        highlightDataList = jdbcTemplate.query(getHighlightDataSql, parameters, new RowMapper<Home>() {
            public Home mapRow(ResultSet rs, int rowNum) throws SQLException {
                Home highlight = new Home ();
                highlight.setId(rs.getInt("id"));
                highlight.setTitle(rs.getString("title"));
                highlight.setUrl(rs.getString("url"));
                highlight.setVotes(rs.getInt("votes"));
                highlight.setDate_created(rs.getString("date_created"));
                highlight.setHours(rs.getString ("hours"));
                highlight.setImage_url(rs.getString("image_url"));
                highlight.setUsername(rs.getString("username"));
                highlight.setUser_pic(rs.getString("picture"));
                highlight.setLiked_user(rs.getString("liked_user"));
                List<String> arr= new ArrayList<>();
                if(rs.getString("tags") !=null && rs.getString("tags")!=null && rs.getString("tags").toString().length()>0)
                {
                    String[] arr1=rs.getString("tags").split(",");
                    arr= Arrays.asList(arr1);
                    highlight.setTags(arr);
                }
                else
                {

                    highlight.setTags(arr );
                }



                highlight.setCopy_url(rs.getString("short_url"));

                return  highlight;
            }
        });

        System.out.println(highlightDataList.size());


        return highlightDataList;


    }



    public String  saveComments(String commentId,String comments)
    {

        try {
            String indertUserSql=" INSERT INTO page_comments (99_page_id,comments) values (:id ,:comments )   ";
            HashMap<String,Object> parameters2 = new HashMap<String,Object>();
            parameters2.put("id",commentId);
            parameters2.put("comments",comments);

            jdbcTemplate.update(indertUserSql, parameters2);
        }
        catch(Exception e){
            e.printStackTrace();
        }




        String getVotesDownSql = "SELECT max(id) as id FROM page_comments";
        List<String> votes = null;
        Map<String, Object> parameters1 = new HashMap<String, Object>();

        votes= jdbcTemplate.query(getVotesDownSql, parameters1, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {

                return rs.getString("id");
            }
        });
        //int numberOfVotesDown=Integer.parseInt(votes.get(0));
        return votes.get(0);



    }

    public  String getCommentsUpdate(String commentId)
    {

        String getVotesDownSql = "SELECT comments   FROM page_comments where id=:id";
        List<String> votes = null;
        Map<String, Object> parameters1 = new HashMap<String, Object>();
        parameters1.put("id",commentId);
        votes= jdbcTemplate.query(getVotesDownSql, parameters1, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {

                return rs.getString("comments");
            }
        });

        return votes.get(0);
    }

    public  String updateComments (String commentId,String comments)
    {

        String saveVoteUpSql = "update page_comments set comments=:comments  where id=:id";


        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("id", commentId);
        parameters.put("comments",comments);

        jdbcTemplate.update(saveVoteUpSql, parameters);

        return "suc";
    }

    public List<String>  getCommentsforPage(String id)
    {



        String getVotesDownSql = "select comments from page_comments where 99_page_id=:id";
        List<String> votes = null;
        Map<String, Object> parameters1 = new HashMap<String, Object>();
        parameters1.put("id", id);
        votes= jdbcTemplate.query(getVotesDownSql, parameters1, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {

                return rs.getString("comments");
            }
        });

        return votes;
    }

//  select (select count(*) from 99_highlights_likes where page_id=a.id and user_id=(select id from user where email='sathiyak@upspring.it'  )) as liked_user,   u.username,u.picture, a.*,(a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,timestampdiff(HOUR, a.date_created, now()) as hours from 99_highlights_page_link a left join user u on u.id=a.user_id left join 99_highlights_likes ls on ls.page_id=a.id    where timestampdiff(HOUR, a.date_created, now()) < 241  and a.deleted<3  and ls.user_id= (select id from user where email='sathiyak@upspring.it') and ls.deleted=0 group by a.url order by a.date_created desc;


}
