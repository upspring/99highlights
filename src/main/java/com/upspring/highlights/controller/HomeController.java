package com.upspring.highlights.controller;

import com.upspring.highlights.dao.HomeDAO;
import com.upspring.highlights.domain.Home;
import com.upspring.highlights.domain.User;
import com.upspring.highlights.service.HomeService;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by Sathya on 15/06/20.
 */
@Controller
public class HomeController {

    @Autowired(required = true)
    private HomeService homeService;

    @Autowired
    private HomeDAO homeDAO;


    @RequestMapping(value = "/oldDesign", method = RequestMethod.GET)
    public String home(HttpServletRequest request, HttpSession session) throws IOException {
        System.out.println("index page");

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);

        List<Home> data= homeService.get99Highlights(decodedEmail);
        session.setAttribute("99highlights",data );
        session.setAttribute("email",decodedEmail);
        session.setAttribute("userName",userName);


       return "index";

    }



    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String cardView(HttpServletRequest request, HttpSession session) throws IOException {
        System.out.println("index page");

        String userPic="";
        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                System.out.println("cookie name--->"+cookie.getName());
                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
                if (cookie.getName().equals("userPic")) {
                    userPic = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);

        List<Home> data= homeService.get99Highlights(decodedEmail);
        session.setAttribute("99highlights",data );
        session.setAttribute("email",decodedEmail);
        session.setAttribute("userName",userName);
        session.setAttribute("userPic",userPic);
        System.out.println("username---->"+userName);


        return "cardView";

    }





    @RequestMapping(value = "/New", method = RequestMethod.GET)
    public String homeNew(HttpServletRequest request, HttpSession session) throws IOException {
        System.out.println("index page");

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);

        List<Home> data= homeService.getNew99Highlights(decodedEmail);
        session.setAttribute("99highlights",data );
        session.setAttribute("email",decodedEmail);
        session.setAttribute("userName",userName);


        return "New";

    }



    @RequestMapping(value = "/MyStories", method = RequestMethod.GET)
    public String myStories(HttpServletRequest request, HttpSession session) throws IOException {
        System.out.println("index page");

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);

        List<Home> data= homeService.getMyStories(decodedEmail);
        session.setAttribute("99highlights",data );
        session.setAttribute("email",decodedEmail);
        session.setAttribute("userName",userName);


        return "MyStories";

    }

    @RequestMapping(value = "/LikedStories", method = RequestMethod.GET)
    public String likedStories(HttpServletRequest request, HttpSession session) throws IOException {
        System.out.println("index page");

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);

        List<Home> data= homeService.getLikedStories(decodedEmail);
        session.setAttribute("99highlights",data );
        session.setAttribute("email",decodedEmail);
        session.setAttribute("userName",userName);


        return "LikedStories";

    }



    @RequestMapping(value = "/SendEmail", method = RequestMethod.GET)
    public String sendEmail(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "toMail", required = true) String toMail, @RequestParam(value = "url", required = true) String pageUrl,   @RequestParam(value = "fromMail", required = true) String fromMail, @RequestParam(value = "title", required = true) String pageTitle,@RequestParam(value="imgUrl")String imgUrl) throws IOException, Exception {

        String decodeTitle = URLDecoder.decode(pageTitle, "UTF-8");
         System.out.println(pageUrl);
         System.out.println("image--->"+imgUrl);
        homeService.sendEmail(toMail, pageUrl, fromMail, decodeTitle,imgUrl);

        return "success";
    }


    @RequestMapping(value="/99Highlights",method = RequestMethod.GET)
    public String highlights(HttpServletRequest request,HttpServletResponse response)
    {

        homeService.get99Highlights("");

        return "";
    }

    @RequestMapping(value="/99HighlightsSaveLikes",method = RequestMethod.GET)
    public  @ResponseBody
    int highlightLikes(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "id", required = true) int id, @RequestParam(value = "emailId", required = true)String emailId)
    {

        int votes= homeService.insert99PageLikes(id,emailId);
        System.out.println("votes--->"+votes);

        return votes;
    }

    @RequestMapping(value="/99HighlightsSaveDisLikes",method = RequestMethod.GET)
    public  @ResponseBody
    int highlightDisLikes(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "id", required = true) int id, @RequestParam(value = "emailId", required = true)String emailId)
    {

        int votes= homeService.insert99PageDisLikes(id,emailId);
        System.out.println("votes--->"+votes);

        return votes;
    }

    @RequestMapping(value="/99HighlightsSaveTags",method = RequestMethod.GET)
     public  @ResponseBody void saveTags(HttpServletRequest request,@RequestParam(value = "id", required = true) int id,@RequestParam(value="tags",required = true) String tags)
    //public  void saveTags(HttpServletRequest request  )
    {

        System.out.println("save tags");
        //String url="https://www.javatpoint.com/spring-tutorial";
        //String tags="spring,java,dependency";
        homeService.save99HighlightsTags(id,tags);


    }


    @RequestMapping(value = "/SocialLogin", method = RequestMethod.GET)
    public
    @ResponseBody
    String socialLogin(@ModelAttribute User user, HttpServletRequest request, HttpSession session, HttpServletResponse response) throws UnsupportedEncodingException, IOException, MalformedURLException, ProtocolException {

        String userParam = request.getParameter("userName");
        String emailId = request.getParameter("email");
        String picture = request.getParameter("picture");

        String userName = URLDecoder.decode(userParam, "UTF8");



        System.out.println("Decoded username" + userName);
        user = homeService.getGmailLoginUserDetail(emailId);


        String color = null;
        if (user != null) {


            homeService.updateUserPicture(emailId, picture);

        } else {
            System.out.println("Email is Not present");
            User newUser = new User();
            newUser.setUsername(userName);
            newUser.setEmailId(emailId);
            newUser.setPicture(picture);
            homeService.addGmailLoginUser(newUser);
            //loginService.SendWelcomeEmail(emailId, userName);
        }
        String result = "success";
        return result;
    }



    @RequestMapping(value="/AddLink",method = RequestMethod.GET)
    public    String addLink(HttpServletRequest request,HttpServletResponse response,@RequestParam(value="url")String url,@RequestParam(value="tags") String tags) throws IOException
    {

        String emailId = "";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {


                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));
        try {

            Document doc = Jsoup.connect(url).userAgent("chrome").get();
            String title = doc.title();
            System.out.println("title--->" + title);

            Element image = doc.select("img").first();
            String imgurl = image.absUrl("src");
            System.out.println("imageurl---->" + imgurl);


            homeService.addLink(url, title, "1", imgurl, tags, decodedEmail);
        }
        catch(Exception e)
        {

        }
        finally
        {


            homeService.addLink(url, "Towards an abridged internet", "1", "http://www.99highlights.com/resources/img/logo-default.png", tags, decodedEmail);

        }



        return "redirect:/";

    }


    @RequestMapping(value="/logout",method = RequestMethod.GET)
    public    String logout(HttpServletRequest request,HttpServletResponse response ) throws IOException
    {

        return "redirect:/";
    }

    @RequestMapping(value="/tagsSearch",method = RequestMethod.GET)
    public    String tagsSearch(HttpSession session,HttpServletRequest request,HttpServletResponse response,@RequestParam(value = "word")String word ) throws IOException
    {

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);


        session.setAttribute("email",decodedEmail);
        session.setAttribute("userName",userName);
       List<Home> t= homeService.getTagsLink(word.trim(),decodedEmail);

        session.setAttribute("99highlights",t );
        return "tagsLink";
    }

    @RequestMapping(value="/99HighlightsDelete",method = RequestMethod.GET)
    public  @ResponseBody  int deleteLink(HttpServletRequest request,HttpSession session,HttpServletResponse response,@RequestParam(value="email")String email,@RequestParam(value="id")String id )
    {


        System.out.println("delete--->"+id+email);
        int delete= homeService.deleteLink(id,email);
        return delete;

    }

    @RequestMapping(value="/getScrollData",method = RequestMethod.GET)
    public  @ResponseBody List<Home> getScrollData(HttpServletRequest request,HttpServletResponse response)
    {
        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        System.out.println("emailId--->"+emailId + decodedEmail);

        List<Home> data= homeService.get99Highlights(decodedEmail);
        return data;


    }


    @RequestMapping(value="/comments",  method = RequestMethod.GET)
    public String comments(HttpServletRequest request,HttpServletResponse response,@RequestParam(value="id") int id)
    {

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {

                if (cookie.getName().equals("GL")) {
                    emailId = cookie.getValue();
                }

                if (cookie.getName().equals("username")) {
                    userName = cookie.getValue();
                }
            }
        }

        String decodedEmail = new String(Base64.decodeBase64(emailId));

        List<Home> data=homeService.getComments(id,decodedEmail);
        request.getSession().setAttribute("comments",data);
        return "commentsNew";
    }



    @RequestMapping(value="/saveComments",method = RequestMethod.GET)
    public  @ResponseBody
    String commentsSave(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "id", required = true) String commentId, @RequestParam(value = "comments", required = true)String  commentList)
    {

        System.out.println("check comments--->"+commentId);
        System.out.println("check comments--->"+commentList);
        String t=homeService.saveComments(commentId,commentList);
        JSONObject jsnobject = new JSONObject(commentList);
        jsnobject.put("commentId",t);
        System.out.println("update--->"+jsnobject);
        System.out.println("update--->"+jsnobject.toString().replace("\\",""));

        homeService.updateComments(t,jsnobject.toString().replace("\\",""));

        return  t;
    }


    @RequestMapping(value="/updateComments",method = RequestMethod.GET)
    public  @ResponseBody
    String commentsUpdate(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "id", required = true) String commentId, @RequestParam(value = "comments", required = true)String  commentList)
    {

        System.out.println("check comments--->"+commentId);
        System.out.println("check comments--->"+commentList);

        homeService.updateComments(commentId, commentList);
        return  " ";
    }


    @RequestMapping(value="/getCommentsforPage",  method = RequestMethod.GET)
    public @ResponseBody  String getComments(HttpServletRequest request,HttpServletResponse response,@RequestParam(value="id") String id)
    {

        String emailId = "";
        String userName="";
        Cookie[] cookies = request.getCookies();


        List<String> St= homeService.getCommentsforPage(id);
        JSONArray jsArray = new JSONArray( );

        if(St.size()>0)
        {
            for( int i=0;i<St.size();i++)
            {

                JSONObject jsonObject = new JSONObject(St.get(i) );
                System.out.println("json object--->"+jsonObject);
                jsArray.put(jsonObject);
            }
        }
        System.out.println("json array"+jsArray);



        return jsArray.toString() ;
    }


    @RequestMapping(value="/listUserStories",method = RequestMethod.GET)
  public  String userStories(HttpServletRequest request,HttpServletResponse res, HttpSession session,@RequestParam(value="id")String emailId)
  {
      String decodedEmail = new String(Base64.decodeBase64(emailId));
      System.out.println("emailId----->"+emailId);
      System.out.println("emailId----->"+decodedEmail);



      String emailId1 = "";
      String userName="";
      Cookie[] cookies = request.getCookies();

      if (cookies != null) {
          for (Cookie cookie : cookies) {

              if (cookie.getName().equals("GL")) {
                  emailId1 = cookie.getValue();
              }

              if (cookie.getName().equals("username")) {
                  userName = cookie.getValue();
              }
          }
      }

      String currentEmail  = new String(Base64.decodeBase64(emailId1));

      System.out.println("emailId--->"+emailId + decodedEmail);

      List<Home> data= homeService.listUserStories(decodedEmail,currentEmail);
      session.setAttribute("99highlights",data );
      session.setAttribute("email",currentEmail);
      session.setAttribute("userName",userName);

      return "listUserStories";

  }


    @RequestMapping(value="/SaveChromeId",method = RequestMethod.GET)
    public  @ResponseBody String saveChromeId(HttpServletResponse req,HttpServletResponse res,@RequestParam(value="id",required = true)String id)
    {

        System.out.println("chrome id---->"+id);
        Cookie ck=new Cookie("LUTWChromeId", id);//creating cookie object
        ck.setMaxAge(60*60*24*365);
        res.addCookie(ck);

        return  "Success";
    }
}
