<%@ page import="com.upspring.highlights.domain.Home" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Sathya
  Date: 15/06/20
  Time: 12:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>99 Highlights</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="<%=request.getContextPath()%>/resources/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<%=request.getContextPath()%>/resources/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<%=request.getContextPath()%>/resources/img/favicon-16x16.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/99styles.css">
    <script src="https://unpkg.com/commentbox.io/dist/commentBox.min.js"></script>


    <style>
        #tags{
            float:left;
            padding:5px;
            font-family:Arial;
        }
        span{
            cursor:pointer;
            display:block;
            float:left;
            color:#fff;
            background-color: #3498DB !important;
            border-radius: 5px;
            padding:4px 10px 4px 8px;
            margin:5px;
            font-size: 13px;
        }
        span:hover{
            opacity:1;
        }
        /* span:after{
           position:absolute;
           content:"×";
           border-radius: 50%;
           padding:1px 5px;
           margin-left:4px;
           font-size:13px;
           background-color:rgba(0, 0, 0, 0.1);
         }
         */
        input{
            background:#eee;
            border:0;
            margin:4px;
            padding:7px;
            width:auto;
        }
        .hidden {
            display: none;
        }


        .commentbox {
            border-top: 1px solid #ddd;
            margin-top: 10px;
            padding: 0;
            margin: 0;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159030309-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-159030309-1');
    </script>
</head>




<div class="nav-custom">
    <nav class="navbar navbar-expand-lg navbar-light  " style="background-color: white">
       <!-- <a class="navbar-brand" href="/"><img src="<%=request.getContextPath()%>/resources/img/logo-6.png" alt="logo-img" style="width: 150px; height: 45px"></a>-->
        <a class="navbar-brand" href="/"><img src="<%=request.getContextPath()%>/resources/img/logo-new-1.png" alt="logo-img" title="Towards an abridged internet" style="width: 190px; height: 50px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <form class="form-inline my-2 my-lg-0" data-toggle="modal" data-target="#login-modal">
                <div id="myBtn" class="btn btn-outline-login my-2 my-sm-0 mr-3" style="display: none">Login</div>
                <div class="btn-group mr-3" id="login" style="display: none">
                    <div class="btn btn-outline-loginusr dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user pr-2"></i>
                        <%=request.getSession().getAttribute("userName")%>
                    </div>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" onclick=" return logout()">Logout</a>
                        <a class="dropdown-item" href="#">About</a>
                    </div>
                </div>
                <!--<button class="btn btn-outline-success my-2 my-sm-0 mr-5" type="submit" id="myBtn">Login</button>-->
                <%

                    if(request.getSession().getAttribute("email").toString().length() <= 0 )
                    {
                %>
                <script>
                    document.getElementById("myBtn").style.display="block";
                </script>
                <%
                    }
                %>


                <%
                    if(request.getSession().getAttribute("email").toString().length() > 0)
                    {
                %>
                <script>
                    document.getElementById("login").style.display="block";
                </script>
                <%
                    }
                %>

                 <div id="add-url" class="btn btn-outline-login my-2 my-sm-0 mr-3">Add URL</div>
            </form>
            <div id="modal-about" class="modal-about">
                <!-- Modal content -->
                <div class="modal-content-about">
                    <p class="close" id="close-about">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>About</h4>
                        </div>
                        <div class="modal-content-body">
                            <div class="about-container">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal-login" class="modal-login">
                <!-- Modal content -->
                <div class="modal-content-login">
                    <p class="close" id="close-google">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>Login</h4>
                        </div>
                        <div class="modal-content-body">
                            <img src="<%=request.getContextPath()%>/resources/img/login-modal-blue.png" alt="google-login" style="height: 110px; width: 300px;" onclick="login()">
                        </div>
                    </div>
                </div>

            </div>
            <div id="modal-add-url" class="modal-login">
                <!-- Modal content -->
                <div class="modal-content-login">
                    <p class="close" id="close-url">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>Add URL</h4>
                        </div>
                        <div class="modal-content-body pl-4 pr-4 pt-2 pb-3">
                            <!--<img src="img/login-modal-blue.png" alt="google-login" style="height: 110px; width: 300px;">-->
                            <p class="pl-3">Enter URL:</p>
                            <input class="ml-3 mb-3" type="text" name="URL"  id="URL" placeholder="Enter URL">
                            <p class="pl-3">Enter Your Tags:</p>
                            <input class="ml-3 mb-2" type="text" name="Tags" placeholder="Enter Your Tags" id="tags-input-url">
                            <input type="submit" class="url-submit mt-3" value="Submit" id="submit-url"   />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </nav>
</div>

<nav class="navbar navbar-expand-lg navbar-dark navbar-survival101 bg-light" style="margin-top: 60px;">
    <div class="container-nav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link  " href="<%=request.getContextPath()%>/">Trending</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/New">New</a>
                </li>

                <%
                    if(request.getSession().getAttribute("email").toString().length() > 0)
                    {
                %>
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/MyStories">MyStories</a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/LikedStories">LikedStories</a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>

</nav>
<div class="container" style="width: 70%;">
    <div class="card-container ml-3" id="main">
        <%
            List<Home> highlightsList = (List<Home>) session.getAttribute("comments");
            System.out.println("jsp list------->"+highlightsList.size());

            if(highlightsList.size()>0)
            {

                System.out.println("list of grid is----->success");


                for ( int i =  0; i < highlightsList.size(); i++ )
                {
                    Home highlights =highlightsList.get(i);

        %>

        <div class="card mt-4  main" id=card_<%=highlights.getId()%>>
            <div class="card-line-number">
            </div>
            <div class="card-header">
                <img src="<%=highlights.getUser_pic()%> "    style="border-radius: 50%" alt="profile-img">
                <h6 class="pl-2 pt-2"><%=highlights.getUsername()%></h6>
                <p class="posted-time pt-2">Posted by <%= highlights.getHours() %> hours ago</p>
            </div>
            <div class="card-body">
                <h3><a href="<%= highlights.getUrl() %>" target="_blank"> <%=highlights.getTitle() %></a></h3>
                <div class="img-container ml-4">
                    <img src="<%=highlights.getImage_url()%>" alt="article-img">
                </div>

                <div class="tag-container mlg-5 mt-2">
                    <div id=tags_<%=highlights.getId()%> class="tags">
                        <h4 style="display: inline-block;" class="tags-header">Tags:</h4>
                        <%
                            System.out.println("tags--->"+    (highlights.getTags().size()>0  )  );
                            if( highlights.getTags().size()  >0 )
                            {

                                for(int j=0;j<highlights.getTags().size();j++)
                                {
                                    System.out.println(highlights.getTags().get(j) );
                        %>
                        <span><%=highlights.getTags().get(j)%>  </span>
                        <%
                                }
                            }
                        %>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="footer-icon">
                    <div class="footer-icon1" data-toggle="tooltip" data-placement="top" title="Like" id=like_<%=highlights.getId()%> onclick="like('<%=highlights.getId()%>')">
                        <img src="<%=request.getContextPath()%>/resources/img/like-black-new-1.png" alt="img" width="22" height="22"  >
                        <p class="like-count" id=like-count_<%=highlights.getId()%> > <%=highlights.getVotes()%> </p>
                        <%
                            String emailId=highlights.getLiked_user();
                            if(request.getSession().getAttribute("email").toString().length() <= 0 )
                            {

                        %>
                        <script>
                            document.getElementById("like_"+<%=highlights.getId()%>).removeAttribute("onclick");
                            //              document.getElementById("like_"+<%=highlights.getId()%>).style.pointerEvents="none";
                            //
                            console.log("inside like befor")
                            $("#like_"+<%=highlights.getId()%>).click(function()
                            {
                                console.log("inside like")
                                var modal = document.getElementById("modal-login");
                                modal.style.display = "block";
                            })

                        </script>
                        <%
                            }

                            if(!highlights.getLiked_user().contains("0")){
                                System.out.println("inside email----");
                        %>
                        <script>

                            document.getElementById("like_"+<%=highlights.getId()%>).firstElementChild.setAttribute("src", "<%=request.getContextPath()%>/resources/img/like-blue-new.png");
                            document.getElementById("like_"+<%=highlights.getId()%>).setAttribute( "onClick", "dislike('<%=highlights.getId()%>')" );

                        </script>
                        <%
                            }
                        %>

                    </div>
                    <div class="footer-icon1" title="Delete" onclick="deleteLink(<%=highlights.getId()%>)" id=delete-link_<%=highlights.getId()%>>
                        <img src="<%=request.getContextPath()%>/resources/img/trash-black-new-1.png" alt="delete-img" width="24" height="24">

                        <%

                            if(request.getSession().getAttribute("email").toString().length() <= 0 )
                            {

                        %>
                        <script>
                            document.getElementById("delete-link_"+<%=highlights.getId()%>).removeAttribute("onclick");
                            // document.getElementById("delete-link_"+<%=highlights.getId()%>).style.pointerEvents="none";
                            console.log("inside delete before ")
                            $("#delete-link_"+<%=highlights.getId()%>).click(function()
                            {
                                var modal = document.getElementById("modal-login");
                                modal.style.display = "block";

                            });
                        </script>
                        <%
                            }
                        %>

                    </div>
                    <!--<div class="footer-icon1" data-toggle="tooltip" data-placement="top" title="Comment"    >
                        <img src="<%=request.getContextPath()%>/resources/img/comment-new-icon.png" alt="img" width="24" height="24">
                    </div>-->
                    <div class="footer-icon1" data-toggle="tooltip" data-placement="top" title="Share on Email" onclick="emailShare('<%=highlights.getImage_url()%>','<%=highlights.getUrl()%>' , '<%=highlights.getTitle().replaceAll("'"," ") %>')">
                        <img src="<%=request.getContextPath()%>/resources/img/email-icon-black.png" alt="img" width="23" height="23">
                    </div>
                    <div class="footer-icon1" data-toggle="tooltip" data-placement="top" title="Tags">
                        <img src="<%=request.getContextPath()%>/resources/img/tags-white.png" alt="img" width="23" height="23" data-toggle="modal" data-target=#myModal_<%=highlights.getId()%> >
                    </div>

                    <%
                        System.out.println("length--->"+highlights.getCopy_url().toString());
                        System.out.println("length--->"+highlights.getCopy_url().toString().trim().length());
                        if(highlights.getCopy_url().toString().length()>1)
                        {
                    %>
                    <div class="footer-icon1" data-toggle="tooltip" data-placement="top" title="Open URL" onclick="openUrl('<%=highlights.getCopy_url()%>')">
                        <img src="<%=request.getContextPath()%>/resources/img/url-white.png" alt="img" width="20" height="20">
                    </div>
                    <%
                        }
                    %>
                    <!-- Modal -->
                    <div class="modal fade" id=myModal_<%=highlights.getId()%> role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="card-line-number-modal"></div>
                                <!--<div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>-->
                                <div class="modal-body ml-2">
                                    <input type="text" placeholder="Enter your tags" id=tags-input_<%=highlights.getId()%>>
                                    <button type="button" value="Submit" data-dismiss="modal" class="modal-submit">Submit</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <script>
                        $(function(){ // DOM ready

                            // ::: TAGS BOX

                            var t='#tags-input_<%=highlights.getId()%>' ;
                            var t1='#tags_<%=highlights.getId()%>>p' ;
                            var id=<%=highlights.getId()%>;
                            $( t).on({

                                focusout : function() {
                                    console.log("this",this);
                                    console.log($(this).parent().parent().parent().parent().attr('id') )
                                    var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters

                                    if(txt) {

                                        var t=0;
                                        $('div#tags_<%=highlights.getId()%>  span').each(function(){

                                            console.log($(this).text());
                                            console.log($(this).text().trim() =="jsoup");
                                            console.log($(this).text() === "jsoup");
                                            if($(this).text().trim().toUpperCase()==txt.toUpperCase())
                                            {
                                                console.log("already exist");
                                                t=1;
                                            }

                                        });
                                        if(t==0)
                                        {
                                            $("<span/>", {text: txt.toLowerCase(), insertBefore: t1});

                                            $.ajax({type:"GET",
                                                url: "/99HighlightsSaveTags?id=" +id + "&tags=" + txt ,
                                                success: function(result){
                                                }})
                                        }

                                    }
                                    this.value = "";

                                },
                                keyup : function(ev) {
                                    // if: comma|enter (delimit more keyCodes with | pipe)
                                    if(/(188|13)/.test(ev.which)) $(this).focusout();
                                }
                            });
                            $(this).on('click', 'span', function(e) {
                                // $(this).remove();
                                e.stopImmediatePropagation();
                                e.preventDefault();
                                console.log("this-->",this);

                                console.log("this.",this.innerHTML);

                                //window.open("http://localhost:8080/tagsSearch?word="+this.innerHTML ,"_self")
                                 window.open("http://www.99highlights.com/tagsSearch?word="+this.innerHTML ,"_self")


                            });

                        });





                    </script>
                </div>
            </div>

            <div class="commentbox" style="margin-left: 30px;"></div>
        </div>



        <%
                }
            }
        %>




    </div>
</div>
</div>


<script>

    //commentBox('');

    commentBox('5716300739379200-proj', {
        className: 'commentbox', // the class of divs to look for
        defaultBoxId: 'commentbox', // the default ID to associate to the div
        tlcParam: 'tlc', // used for identifying links to comments on your page
        backgroundColor: null, // default transparent
        textColor: 'black', // default black
        subtextColor: null, // default grey
        singleSignOn: null, // enables Single Sign-On (for Professional plans only)




    });
</script>





<script>

    function openUrl(url )
    {

        window.open(url,"_blank");
    }

</script>

<script>

    function emailShare(imgUrl,url,title){
        var email='<%=request.getSession().getAttribute("email")%>';
        var email_send = prompt("Enter E-mail id ", email);

        if (email_send == null || email_send == "") {

        }
        else
        {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                //url: "http://localhost:8080/SendEmail?toMail="+email_send +"&url="+ encodeURIComponent(url) +  "&fromMail="+email +"&title="+encodeURIComponent(title)+"&imgUrl="+encodeURIComponent(imgUrl),
                  url: "http://www.99highlights.com/SendEmail?toMail=" + email_send + "&url=" + encodeURIComponent(url) +"&fromMail=" + email + "&title=" + encodeURIComponent(title)+"&imgUrl="+encodeURIComponent(imgUrl),
                success: function (data) {

                    clearSelection();
                }
            });
        }

    }

</script>

<script>



    function like(id)
    {
        console.log("here"+id);

        document.getElementById("like_"+id).removeAttribute("onclick");



        var email = '<%=request.getSession().getAttribute("email")%>';
        console.log("user email"+email);

        $.ajax({type:"GET",
            url: "/99HighlightsSaveLikes?id=" +id + "&emailId=" + email ,
            success: function(result){
                console.log("result"+result);
                document.getElementById("like-count_"+id).innerHTML=result;
                document.getElementById("like_"+id).firstElementChild.setAttribute("src","<%=request.getContextPath()%>/resources/img/like-blue-new.png");
                document.getElementById("like_"+id).setAttribute( "onClick", "dislike("+id+" )" );
                //document.getElementById("like_"+id).style="cursor:none";
                //document.getElementById("like_"+id).style.pointerEvents="none";




            }})
    }

    function dislike(id)
    {


        document.getElementById("like_"+id).removeAttribute("onclick");



        var email = '<%=request.getSession().getAttribute("email")%>';
        console.log("user email"+email);

        $.ajax({type:"GET",
            url: "/99HighlightsSaveDisLikes?id=" +id + "&emailId=" + email ,
            success: function(result){
                console.log("result"+result);
                document.getElementById("like-count_"+id).innerHTML=result;
                document.getElementById("like_"+id).firstElementChild.setAttribute("src","<%=request.getContextPath()%>/resources/img/like-black-new-1.png");
                document.getElementById("like_"+id).setAttribute( "onClick", "like("+id+" )" );
                //document.getElementById("like_"+id).style="cursor:none";
                //document.getElementById("like_"+id).style.pointerEvents="none";




            }})
    }

</script>


<script>
    function logout()
    {
        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.cookie = "GL=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.getElementById("myBtn").style.display="block";
        document.getElementById("login").style.display="none";
       // window.open("http://localhost:8080/logout","_self");
         window.open("http://www.99highlights.com/logout","_self");

    }

</script>

<script>

    function login()
    {

        //window.open("https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://localhost:8080/oauth2callback&response_type=code&client_id=420424849668-t6e1k7em1erhpi4rnvf705g5l4gjifj7.apps.googleusercontent.com&approval_prompt=force","_self" );
         window.open("https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://www.99highlights.com/oauth2callback&response_type=code&client_id=1052984239121-vt400kdkvaedlqv7s5o4aqia0cdgprpu.apps.googleusercontent.com&approval_prompt=force","_self" );
    }
</script>


<script>
    function deleteLink(id)
    {

        var email='<%=request.getSession().getAttribute("email")%>'
        $.ajax({type:"GET",
            url: "/99HighlightsDelete?id=" +id + "&email=" + email ,
            success: function(result){
                console.log(result)
                if(result ==3)
                {
                    document.getElementById("delete-link_"+id).style.display="none";
                    document.getElementById("card_"+id).style.display="none";
                }
            }})

    }
</script>

<script>

    function comments(id) {
        //window.open("http://localhost:8080/comments?id="+id,"_blank");
        window.open("http://www.99highlights.com/comments?id="+id,"_blank");
    }
</script>

<script>
    // Get the modal
    console.log('inside of modal-login');
    var modal = document.getElementById("modal-login");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementById("close-google") ;

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        console.log('inside of btn');
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>


<script>
    // Get the modal

    document.getElementById("add-url").addEventListener("click",function () {


        var emailId='<%=request.getSession().getAttribute("email")%>' ;

        console.log("emailId",emailId);
        console.log(emailId.toString().length);

        if(emailId.length>0  ) {
            console.log('inside of modal add url');
            var modal1 = document.getElementById("modal-add-url");

            // Get the button that opens the modal
            var btn = document.getElementById("add-url");

            modal1.style.display = "block";
            var submit_btn = document.getElementById("submit-url");
            // Get the <span> element that closes the modal
            var span = document.getElementById("close-url");

            // When the user clicks the button, open the modal
            btn.onclick = function () {
                console.log('inside of btn');
                modal1.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal1.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal1.style.display = "none";
                }
            }

            submit_btn.onclick = function () {
                console.log('iniside of submit');
                modal1.style.display = "none";

                var url = document.getElementById("URL").value;
                var tags = document.getElementById("tags-input-url").value;

                console.log("url--->", url);
                console.log("tags--->", tags);
                window.location.href = '/AddLink?url=' + url + '&tags=' + tags;


            }
        }
        if(emailId <=0 )
        {
            var modal = document.getElementById("modal-login");
            modal.style.display = "block";
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
    })



</script>

<script>
    $(document).ready(function(){

        var chrome_id;

        var cookiestring  = document.cookie;
        var cookiearray = cookiestring.split(';');
//        console.log(cookiearray);
        for(var i =0 ; i < cookiearray.length ; ++i){

            if(cookiearray[i].trim().match('^'+"LUTWChromeId"+'=')){
                console.log( cookiearray[i].replace("LUTWChromeId=",'').trim());
                chrome_id=cookiearray[i].replace("LUTWChromeId=",'').trim();
            }



        }

        if(chrome_id!="")
        {
            //     document.getElementById("chromeId").style.display="none"
        }




    });
</script>




<script>
    // Get the modal
    console.log('inside of modal add url');
    var modal1 = document.getElementById("modal-about");

    // Get the button that opens the modal
    var btn = document.getElementById("about");

    var submit_btn = document.getElementById("submit-tags");
    // Get the <span> element that closes the modal
    var span = document.getElementById("close-url");

    var close_tabs = document.getElementById('close-about');

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        console.log('inside of btn');
        modal1.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal1.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal1.style.display = "none";
        }
    }

    submit_btn.onclick = function(){
        console.log('iniside of submit');
        modal1.style.display="none";
    }
    close_tabs.onclick = function(){
        console.log('iniside of submit');
        modal1.style.display="none";
    }
</script>

</html>