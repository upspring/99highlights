<%@ page import="com.upspring.highlights.domain.Home" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>99 Higlights</title>
    <link rel="apple-touch-icon" sizes="180x180" href="img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style-comment.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/comments-lib.css">
    <!--<script src="https://unpkg.com/commentbox.io/dist/commentBox.min.js"></script>-->

    <style>
        .branding {
            display: none !important;
        }
        .commentbox {
            border-top: 1px solid #ddd;
            margin-top: 10px;
            padding: 0;
            margin: 0;
        }
        .commentbox img {
            display: none !important;
        }
        iframe div {
            color: black;
        }
        #hyvor-talk-view{
            margin-left: 30px;
        }
        .comment-writer-view .textarea {
            height: 50px !important;
        }
        .comment-box>.header .left h2 {
            margin-left: 200px !important;
            float: right !important;
        }
        #content-loader {
            display:block;
        }
        #main-content {
            display: none;
        }
    </style>
</head>
<body>
<div class="nav-custom">
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: white;">
        <a class="navbar-brand" href="/"><img src="<%=request.getContextPath()%>/resources/img/logo-new-1.png" alt="logo-img" title="Towards an abridged internet" style="width: 190px; height: 50px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <form class="form-inline my-2 my-lg-0" data-toggle="modal" data-target="#login-modal">
                <!--<button class="btn btn-outline-success my-2 my-sm-0 mr-5" type="submit" id="myBtn">Login</button>-->
                <!--   <div id="add-url" class="btn btn-outline-login my-2 my-sm-0 mr-3">Add URL</div>-->
                <!--<div id="login-usr" class="btn  mr-3">
                    <p>mthaha</p>
                </div>-->
                <div class="btn-group mr-3">
                    <div class="btn btn-outline-loginusr dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user pr-2"></i>
                        <%=request.getSession().getAttribute("userName")%>
                    </div>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Logout</a>
                        <a class="dropdown-item" href="#" id="about">About</a>
                    </div>
                </div>
                <!-- <div id="myBtn" class="btn btn-outline-login my-2 my-sm-0 mr-3">Login</div>-->
              <!--  <div class="btn btn-outline-ext my-2 my-sm-0 mr-3"><a href="https://chrome.google.com/webstore/detail/light-up-the-web/afabncndnlipbonmgbjjcfddjhpmlgeo?utm_source=gmail" target="_blank"><img src="img/chrome-icon.png" alt="chrome-icon" width="30px"; height="20px" class="pr-2">Get Extension</a></div>-->

            </form>
            <div id="modal-about" class="modal-about">
                <!-- Modal content -->
                <div class="modal-content-about">
                    <p class="close" id="close-about">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>About</h4>
                        </div>
                        <div class="modal-content-body">
                            <div class="about-container">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal-login" class="modal-login">
                <!-- Modal content -->
                <div class="modal-content-login">
                    <p class="close">x</p>
                    <div class="modal-login-content">
                        <div class="modal-content-header" style="background-color: #222">
                            <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                            <h4>Login</h4>
                        </div>
                        <div class="modal-content-body">
                            <img src="<%=request.getContextPath()%>/resources/img/login-modal-blue.png" alt="google-login" style="height: 110px; width: 300px;">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="modal-add-url" class="modal-login">
            <!-- Modal content -->
            <div class="modal-content-login">
                <p class="close" id="close-url">x</p>
                <div class="modal-login-content">
                    <div class="modal-content-header" style="background-color: #222">
                        <!--<img src="img/logo-modal.png" alt="modal-header-img" style="height: 50px; width:150px;">-->
                        <h4>Add URL</h4>
                    </div>
                    <div class="modal-content-body pl-4 pr-4 pt-2 pb-3">
                        <!--<img src="img/login-modal-blue.png" alt="google-login" style="height: 110px; width: 300px;">-->
                        <p class="pl-3">Enter URL:</p>
                        <input class="ml-3 mb-3" type="text" name="URL" placeholder="Enter URL">
                        <p class="pl-3">Enter Your Tags:</p>
                        <input class="ml-3 mb-2" type="text" name="Tags" placeholder="Enter Your Tags" id="tags-input-url">
                        <input type="submit" class="url-submit mt-3" value="Submit" id="submit-url"></input>
                    </div>
                </div>
            </div>

        </div>
</div>

<nav class="navbar navbar-expand-lg navbar-dark navbar-survival101 bg-light" style="margin-top: 60px;">
    <div class="container-nav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link active " href="<%=request.getContextPath()%>/">Trending</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="<%=request.getContextPath()%>/New">New</a>
                </li>

                <%
                    if(request.getSession().getAttribute("email").toString().length() > 0)
                    {
                %>
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/MyStories">My Stories</a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/LikedStories">Liked Stories</a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</nav>
<div class="container mg-top">
    <div class="d-flex justify-content-center align-item-center"></div>
    <div class="row">
        <div class="comment-container" id="main-content">
            <%
                List<Home> highlightsList = (List<Home>) session.getAttribute("comments");
                System.out.println("jsp list------->"+highlightsList.size());

                if(highlightsList.size()>0)
                {

                    System.out.println("list of grid is----->success");


                    for ( int i =  0; i < highlightsList.size(); i++ )
                    {
                        Home highlights =highlightsList.get(i);

            %>
            <div class="comment-header">
                <a href="<%=highlights.getUrl()%>"><h2><%=highlights.getTitle()%></h2></a>
            </div>
            <div class="publisher-info">
                <img src="<%=highlights.getUser_pic()%>" alt="Publisher-img" height="40px" width="40px">
                <span class="publisher-name"><%=highlights.getUsername()%></span>
                <div class="published-date"><%=highlights.getDate_created()%></div>
            </div>
            <div class="comment-img-1">
                <img src=" <%=highlights.getImage_url()%>" alt="header-img">
            </div>
            <div class="tags-list ml-3 mt-5">
                <p class="tags-listed"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="28" height="28"
                                            viewBox="0 0 172 172"
                                            style=" fill:#000000;">
                    <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                        <path d="M0,172v-172h172v172z" fill="none"></path>
                        <g fill="#777777">
                            <path d="M85.355,6.88c-3.07719,0.13438 -6.00656,0.73906 -8.2775,3.01l-67.51,67.51c-1.70656,1.70656 -2.6875,4.03125 -2.6875,6.45c0,2.41875 0.98094,4.63594 2.6875,6.3425l11.395,11.395c0.57781,-3.17125 1.97531,-6.06031 4.3,-8.385l67.6175,-67.51c4.99875,-5.01219 11.59656,-5.0525 15.5875,-5.0525h36.0125v-2.9025c0,-5.99312 -4.86437,-10.8575 -10.8575,-10.8575h-45.15c-1.03469,0 -2.09625,-0.04031 -3.1175,0zM105.995,27.52c-3.07719,0.13438 -6.00656,0.73906 -8.2775,3.01l-67.51,67.51c-1.70656,1.70656 -2.6875,4.03125 -2.6875,6.45c0,2.41875 0.98094,4.63594 2.6875,6.3425l51.6,51.6c1.70656,1.70656 3.92375,2.6875 6.3425,2.6875c2.41875,0 4.74344,-0.98094 6.45,-2.6875l67.51,-67.51c3.03688,-3.03687 3.02344,-7.28312 3.01,-11.395v-4.4075c0,-8.77469 0,-31.90062 0,-38.8075v-1.935c0,-5.99312 -4.86437,-10.8575 -10.8575,-10.8575h-45.15c-1.03469,0 -2.09625,-0.04031 -3.1175,0zM137.6,48.16c1.76031,0 3.49375,0.69875 4.8375,2.0425c2.6875,2.6875 2.6875,6.9875 0,9.675c-2.6875,2.6875 -6.9875,2.6875 -9.675,0c-2.6875,-2.6875 -2.6875,-6.9875 0,-9.675c1.34375,-1.34375 3.07719,-2.0425 4.8375,-2.0425z"></path></g></g></svg>
                    <%
                        System.out.println("tags--->"+    (highlights.getTags().size()>0  )  );
                        if( highlights.getTags().size()  >0 )
                        {

                            for(int j=0;j<highlights.getTags().size();j++)
                            {
                                System.out.println(highlights.getTags().get(j) );
                    %>

                    <i class="tags-outline-new"><%=highlights.getTags().get(j) %></i>
                <%
                    }
                    }
                %>
            </p>
            </div>
            <div class="comment-area" style="border-top: 1px solid #666">
                <!--  <div class="commentbox" style="margin-top: 30px;"></div>-->
                <form onsubmit="event.preventDefault();">
                    <p class="comment-count">Comments </p>
                    <div class="comment-label">
                        <label class="">Comment</label>
                    </div>
                    <textarea name="joinDiscussion" id="joinDiscussion" rows="4" cols=70 placeholder="" class="input-comment"></textarea>
                    <div id="postComment">
                        <label style="visibility: hidden;">UserName:</label>
                        <input type="input" id="userName" value="<%=request.getSession().getAttribute("userName")%> " style="visibility: hidden;" />
                        <input type="input" id="imgUrl" value="<%=request.getSession().getAttribute("userPic")%>" style="visibility: hidden;" />
                        <input type="input" id="commentId" value="<%=highlights.getId()%> " style="visibility: hidden;" />
                        <input type="button" value="Post Comment" id="post" class="btn-submit" onclick="myFunction()">
                    </div>
                    <section id="viewComments" class="comments-section"></section>
                </form>
            </div>
        </div>








        <div class="comment-container" id="content-loader">
            <div class="comment-header mb-4">
                <div class="art-header-cnt"></div>
                <div class="art-header-cnt"></div>
                <div class="art-header-cnt"></div>
            </div>
            <div class="publisher-info">
                <div class="tags-cnt-rounded"></div>
                <div class="tags-cnt-user"></div>
                <div class="tags-cnt-date"></div>
                <!--<img src="img/nature.png" alt="Publisher-img" height="40px" width="40px">
                <span class="publisher-name">Mohamed Thaha</span>
                <div class="published-date">Jul 7 2020</div>-->
            </div>
            <div class="comment-img-1 pr-4 pl-4">
                <div class="card-header-img-cnt1"> </div>
            </div>
            <div class="tags-list ml-3 mt-5">
                <div class="tags-cnt-header"></div>
                <div class="tags-cnt"></div>
                <div class="tags-cnt"></div>
                <div class="tags-cnt"></div>
                <div class="tags-cnt"></div>
            </div>
            <div class="card-footer" style="background-color: #f7f7ff;">
                <div class="card-footer-rounded-cnt" ></div>
                <div class="card-footer-content-cnt"></div>
                <div class="card-footer-cmt-cnt"></div>
                <div class="card-footer-submit-cnt"></div>
                <div class="ml-5 mt-5">
                    <div class="card-footer-rounded-cnt"></div>
                    <div class="card-footer-content-cnt"></div>
                    <div class="card-footer-cmt-cnt"></div>
                    <div class="card-footer-submit-cnt"></div>
                </div>
                <br/><br/>
            </div>
        </div>

        <%
            }
            }
        %>
    </div>
</div>


<script>
    $(document).ready(function() {
        console.log('inside of jquery');
        /*var myVar;
         myVar = setTimeout(showPage, 3000);
         function showPage() {
         console.log('inside of settimeout');
         document.getElementById("cc").style.display = "none";
         document.getElementById("dd").style.display = "block";
         }*/
        function explode(){
            console.log('inside of explode');
            document.getElementById("content-loader").style.display = "none";
            document.getElementById("main-content").style.display = "block";
        }
        setTimeout(explode, 1000);
    });

</script>
<script>

    //commentBox('');

    commentBox('5716300739379200-proj', {
        className: 'commentbox', // the class of divs to look for
        defaultBoxId: 'commentbox', // the default ID to associate to the div
        tlcParam: 'tlc', // used for identifying links to comments on your page
        backgroundColor: null, // default transparent
        textColor: 'black', // default black
        subtextColor: null, // default grey
        singleSignOn: null, // enables Single Sign-On (for Professional plans only)
        /**
         * Creates a unique URL to each box on your page.
         *
         * @param {string} boxId
         * @param {Location} pageLocation - a copy of the current window.location
         * @returns {string}
         */
        createBoxUrl(boxId, pageLocation) {

            pageLocation.search = ''; // removes query string!
            pageLocation.hash = boxId; // creates link to this specific Comment Box on your page
            return pageLocation.href; // return url string
        },
        /**
         * Fires once the plugin loads its comments.
         * May fire multiple times in its lifetime.
         *
         * @param {number} count
         */
        onCommentCount(count) {

        }

    });
</script>
<script>
    window.addEventListener('beforeunload', function(event) {
        console.log("befor unload");
        localStorage.clear();
    }, false);
</script>
<script src="<%=request.getContextPath()%>/resources/js/comment.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
