( function(){

    var comment_id;
    var imgUrl;
    var username;
    var main_comment_id;

    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
    var dateObj = new Date();
    var month = monthNames[dateObj.getMonth()];
    var day = String(dateObj.getDate()).padStart(2, '0');
    var year = dateObj.getFullYear();
    var output = day  + ' '+ month  + ' ' + year;

    console.log(output);
    function Comment(userName,text,votes,commentList,commentId,imgUrl,date){
        this.userName = userName;
        this.text = text;
        this.votes = votes;
        this.commentId=commentId;
        this.imgUrl=imgUrl;
        this.commentList = commentList
        this.date=output;
    }



    Comment.prototype.save = function(){
        var commentList = JSON.parse(window.localStorage.getItem(comment_id))||[];
        console.log("comm save",commentList);
        console.log(this);
        commentList.push(this);
        createCommentView(commentList)
    }

    Comment.prototype.updateReplyList = function(){
        var commentList = JSON.parse(window.localStorage.getItem(comment_id))||[];
        // search for that comment in the list

        commentList = findAndUpdateComment(commentList,this)
        for(var t=0;t<commentList.length;t++ )
        {
            if(commentList[t].commentId==this.commentId)
            {
                console.log(this);

                $.ajax({type:"GET",
                    url: "http://www.99highlights.com/updateComments?id=" +this.commentId+"&comments="+encodeURIComponent( JSON.stringify(commentList[t])),
                    success: function(result){
                        console.log(result);

                    }})
            }
        }
        createCommentView(commentList)
    }

    function findAndUpdateComment(commentList,comment){

        for(var i = 0; i<commentList.length;i++){
            if(commentList[i].text == comment.text && commentList[i].userName == comment.userName)
                commentList[i] = comment
            if(commentList[i].commentList.length>0)
                findAndUpdateComment(commentList[i].commentList,comment)
        }
        return commentList;
    }

    function createCommentView(commentList){
        var docFrag = document.createDocumentFragment();
        docFrag.appendChild(showComments(commentList));
        document.getElementById('viewComments').innerHTML = "";
        document.getElementById('viewComments').appendChild(docFrag);



        commentData=JSON.stringify(commentList);

        window.localStorage.setItem(comment_id,JSON.stringify(commentList));

    }

    function createComment(userName,text,votes,commentId,imgUrl){

        var comment = new Comment(userName,text,votes,[],commentId,imgUrl,"");
        var id;
        console.log(commentId);
        $.ajax({type:"GET",
            url: "http://www.99highlights.com/saveComments?id=" +comment_id+"&comments="+encodeURIComponent( JSON.stringify(comment)),
            success: function(result){

                comment.commentId=result;
                main_comment_id=result;
                id=result;
                console.log("comment ajas",comment );
                console.log("main comment id--->",main_comment_id)

                comment.updateReplyList();

            }})

        comment.save();
        // comment.commentId=id;
        //createCommentView(comment);




        return comment
    }

    function showComments(commentList){

        var mainUL = document.createElement("ul");
        mainUL.className="comment-ul"
        for(var i=0;i<commentList.length;i++){
            var comment = new Comment(commentList[i].userName,commentList[i].text,commentList[i].votes, commentList[i].commentList,commentList[i].commentId,commentList[i].imgUrl,commentList[i].date)
            var li = createLi(comment,i)
            mainUL.appendChild(li)
            if(commentList[i].commentList.length>0){
                mainUL.appendChild(showComments(commentList[i].commentList))
            }
        }
        return mainUL
    }

    function createLi(comment,index){
        // main li element
        var li = document.createElement("li");

        // main div for the li element
        var mainDiv = document.createElement("div");
        mainDiv.id="main-div";
        mainDiv.className="box-comment"

        //commentDiv which will have comment and username
        var commentDiv = document.createElement("div");
        commentDiv.id="comment-div";

        // comment Div Header
        var commentDivHeader = document.createElement('div');
        commentDivHeader.className = "comment-div-header";
        var commentDivUser = document.createElement('p');
        commentDivUser.className="comment-div-user";
        commentDivUser.innerText=comment.userName;
        var commentDivUserIcon = document.createElement('img');
        commentDivUserIcon.className="comment-user-icon";
        commentDivUserIcon.src=comment.imgUrl;
        var commentDivTime = document.createElement('div');
        commentDivTime.className="comment-div-time";
        commentDivTime.innerHTML=comment.date;

        commentDivHeader.appendChild(commentDivUserIcon);
        commentDivHeader.appendChild(commentDivUser);
        commentDivHeader.appendChild(commentDivTime);

        var commentDivBody = document.createElement('div');
        commentDivBody.className="comment-div-body";
        var commentDivComment = document.createElement('p');
        commentDivComment.className="comment-div-comment";
        commentDivComment.innerText=comment.text;

        var commentDivReplyIcon = document.createElement('i');
        commentDivReplyIcon.className="reply-icon";
        commentDivReplyIcon.id="comment-reply"
        commentDivReplyIcon.innerHTML=" Reply";
        commentDivReplyIcon.onclick = function(){
            commentDivReplyIcon.style.cssText = 'display:none';
            commentDivFooter.style.cssText = 'display:block';

        }

        commentDivBody.appendChild(commentDivComment);
        commentDivBody.appendChild(commentDivReplyIcon);

        // Comment Div Footer
        var commentDivFooter = document.createElement('div');
        commentDivFooter.id="comment-div-footer";
        commentDivFooter.className="comment-div-footer";

        var commentDivForm = document.createElement('div');
        commentDivForm.id="comment-div-form";
        commentDivForm.className = "comment-div-form";
        var commentDivFormText = document.createElement('textarea');
        commentDivFormText.className="comment-div-form-text";
        commentDivFormText.rows="4";
        commentDivFormText.cols="40";
        var commentDivFormSubmit = document.createElement('button');
        commentDivFormSubmit.id="comment-div-form-submit";
        commentDivFormSubmit.className="comment-div-form-submit";
        commentDivFormSubmit.innerHTML="Reply";
        commentDivFormSubmit.onclick= function(){
            var content = commentDivFormText.value
            var user = username;

            var reply = new Comment(user,content,0,[],comment.commentId,imgUrl,"")
            console.log(comment);
            console.log(comment.commentList);
            console.log("eply--->",reply);


            comment.commentList.push(reply)
            console.log("after update 1" ,comment);
            comment.updateReplyList()
            console.log("after update 2" ,comment);


        }


        commentDivForm.appendChild(commentDivFormText);
        commentDivForm.appendChild(commentDivFormSubmit);
        commentDivFooter.appendChild(commentDivForm);



		/*var commentNameAndText = document.createTextNode(comment.userName+": " + comment.text);
		 commentDiv.appendChild(commentNameAndText);*/
        commentDiv.appendChild(commentDivHeader);
        commentDiv.appendChild(commentDivBody);
        commentDiv.appendChild(commentDivFooter);




        mainDiv.appendChild(commentDiv)
        li.appendChild(mainDiv)
        return li;
    }

    document.getElementById('post').addEventListener('click',function(){
        var userName = document.getElementById('userName').value;

        var commentId = document.getElementById('commentId').value;

        var content = document.getElementById('joinDiscussion').value;

        createComment(username,content,0,0,imgUrl);
        document.getElementById('joinDiscussion').value="";
    })

    comment_id = document.getElementById('commentId').value;
    imgUrl = document.getElementById('imgUrl').value;
    username = document.getElementById('userName').value;
    var commentList=[];
    var commentData;

    $.ajax({type:"GET",
        url: "http://www.99highlights.com/getCommentsforPage?id=" +comment_id ,
        success: function(result){
            commentList=JSON.parse( result );

            if(commentList.length)
                createCommentView(commentList)

        }})



})()